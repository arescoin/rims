/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.common;

import java.util.logging.Logger;

/**
 * Перечисление кодов выхода из приложения
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ExitForCode.java 60 2010-11-15 12:14:01Z dgomon $"
 */
public enum ExitForCode {

    ok("Status: OK"),

    errorArguments("Status: Incorrect program arguments"),
    errorConfig("Status: Incorrect configuration"),
    errorStartUpSynchronization("Status: Error while reading data"),
    errorInitialization("Status: Application initializing error");
    private String stdMsg;

    private Logger logger = Logger.getLogger(ExitForCode.class.getName());

    /**
     * Конструирует код выхода
     *
     * @param standardMsg сообщение в консоль
     */
    ExitForCode(String standardMsg) {
        stdMsg = standardMsg;
    }

    /**
     * Прерывает закрывает приложение
     */
    public void exit() {
        this.exit(null);
    }


    /**
     * Закрывает приложение с сообщением
     *
     * @param message сообщение
     */
    public void exit(String message) {

        logger.info(this.stdMsg);
        if (message != null && message.length() > 0) {
            logger.info("Custom msg: " + message);
        }

        System.exit(this.ordinal());
    }

}
