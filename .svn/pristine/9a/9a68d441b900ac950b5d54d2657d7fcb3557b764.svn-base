/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import ru.xr.cache.Cache;
import ru.xr.cache.RegionNotFoundException;
import ru.xr.rims.services.Connection;
import ru.xr.rims.services.StorageFilter;
import ru.xr.rims.services.TimeoutException;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Реализация по умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DefaultConnection.java 128 2011-10-21 15:14:00Z dgomon $"
 */
@SuppressWarnings("WeakerAccess")
public class DefaultConnection implements Connection, Serializable {

    private static Logger logger = Logger.getLogger(DefaultConnection.class.getName());

    /** Свободные подключения */
    private final static HashSet<Long> FREE_CONNECTIONS = new HashSet<>(100);

    /** Содержит существующие сессии */
    private final static Map<Long, DefaultConnection> CONNECTIONS =
            new ConcurrentHashMap<>();

    /** Значение используемое для индикатора стартовой позиции в транзакции */
    private static final int CLEAR_POSITION_VALUE = -1;
    /** Идентификатор сессии */
    private final Long id;
    /** Хранилище камманд в рамках транзакции */
    private final ArrayList<Action> actions = new ArrayList<>();
    /** Индикатор текущей позиции в транзакции */
    private int currentPosition = CLEAR_POSITION_VALUE;

    /** Признак заморозки транзакции */
    private boolean freeze = false;

    /** Локальный аналог кеша, для хранения временных данных, обеспечения их доступности в пределах сессии */
    private ConnectionsLocalCache localCache = new ConnectionsLocalCache();
    /** Ключи зачишенных регионов */
    private Set<Object> ignoredRegionsContent = new HashSet<>();
    /** Ключи удаленных регионов */
    private Set<Object> deletedRegions = new HashSet<>();
    /** Ключи удаленных объектов по регионам */
    private Map<Object, Set<Object>> deletedObjects = new HashMap<>();


    /** Конструирует новый инстан соединения (сессии) и помещает его в реестр */
    protected DefaultConnection() {
        synchronized (CONNECTIONS) {
            this.id = (Long) getNewId();
            CONNECTIONS.put(this.id, this);
        }
    }

    /**
     * Генерирует уникальный идентификатор сессии
     *
     * @return идентификатор
     */
    private static Object getNewId() {

        Long id = System.nanoTime();

        while (CONNECTIONS.containsKey(id)) {
            id = System.nanoTime();
        }

        return id;
    }

    /**
     * Возвращает идентификатор сессии
     *
     * @return идентификатор
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Возвращает сессию по переданному идентификатору, если сессии нет, вернется null
     *
     * @param id идентификатор сессии
     * @return сессия
     */
    public static DefaultConnection getConnection(Object id) {
        final Long tmpId = (Long) id;
        return CONNECTIONS.get(tmpId);
    }

    /**
     * Возвращает идентификатор свободной сессии
     *
     * @return идентификатор из пула
     */
    public static Long getFreeConnectionId() {
        Long o;

        synchronized (FREE_CONNECTIONS) {
            if (!FREE_CONNECTIONS.isEmpty()) {
                o = FREE_CONNECTIONS.iterator().next();
                FREE_CONNECTIONS.remove(o);
            } else {
                final DefaultConnection defaultConnection = new DefaultConnection();
                CONNECTIONS.put(defaultConnection.getId(), defaultConnection);
                o = defaultConnection.getId();
            }
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Return connection: " + o);
        }

        return o;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Далее следуют методы сессионного контекста
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Выставить и зарегистрировать на экшене лок
     *
     * @param details доп. инфо для опозанания запроса на блокировку и самой блокировки
     * @param region  регион
     * @param key     ключь объекта
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    protected void lock(Object details, Object region, Object key) throws TimeoutException {
        Lock.lock(details, getId(), region, key, Lock.LOC_REQUEST_TIMEOUT, true);
    }


    /**
     * Снять лок, если он был...
     *
     * @param region регион хранения
     * @param key    ключ оббъекта
     */
    protected void unLock(Object region, Object key) {
        Lock.unLock(getId(), region, key, Lock.LOC_REQUEST_TIMEOUT, true);
    }

    @Override
    public synchronized void commit() throws Exception {
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Start commit " + getId());
        }

        freeze = true;
        synchronized (actions) {
            for (Action action : actions) {
                action.execute();
                currentPosition++;
            }
            clear();
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "End commit " + getId());
        }
    }

    @Override
    public synchronized void rollback() {
        logger.info("Start rollback " + getId());

        synchronized (actions) {
            while (currentPosition > CLEAR_POSITION_VALUE) {
                actions.get(currentPosition).rollback();
                currentPosition--;
            }
            clear();
        }

        logger.info("End rollback " + getId());
    }

    /** Сбрасывает состояние сессии в исходное */
    void clear() {

        for (Action action : actions) {
            action.clear();
        }

        deletedObjects.clear();
        deletedRegions.clear();
        ignoredRegionsContent.clear();

        actions.clear();
        currentPosition = CLEAR_POSITION_VALUE;
        freeze = false;
        localCache = new ConnectionsLocalCache();
    }

    @Override
    public void close() {

        synchronized (actions) {
            clear();
        }

        synchronized (FREE_CONNECTIONS) {
            FREE_CONNECTIONS.add(this.id);
        }
    }

    // работа с данными, аналоги бизнес-методов ////////////////////////////////////////////////////////////////////////

    Object get(Object region, Object key) {
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Get value by region: " + region + ", key: " + key + ", on: " + getId());
        }

        Object result = localCache.get(region, key);
        if (result == null) {
            if ((!deletedObjects.containsKey(region) || !deletedObjects.get(region).contains(key))
                    && !deletedRegions.contains(region) && !ignoredRegionsContent.contains(region)) {
                result = Cache.get(region, key);
            }
        }

        return result;
    }

    @SuppressWarnings({"unchecked"})
    Map get(Object region) {
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Get value by region: " + region + ", on: " + getId());
        }

        Map result = localCache.getRegionContent(region);

        if (!deletedRegions.contains(region) && !ignoredRegionsContent.contains(region)) {
            result.putAll(Cache.get(region));

            if (deletedObjects.containsKey(region)) {
                for (Object o : deletedObjects.get(region)) {
                    result.remove(o);
                }
            }
        }

        return result;
    }

    /**
     * Производит поиск значений в хранилище по переданным критериям содержащимся в {@link StorageFilter}
     * <p/>
     * Последовательность работы:
     * <ul>
     * <li>проверяет списки удаленных регионов,<br>
     * если переданный регион не в списках - идет дальше, иначе - вернется пустая колекция</li>
     * <li>проверяет списки зачищенных регионов,<br>
     * если переданный регион не в списках - идет дальше, иначе - вернется пустая колекция</li>
     * <li>получает содержимое основного и сессионного кешей, суммирует их</li>
     * <li>из полученного резльтата вычитает то что в сессии помечено на удаление</li>
     * <li>отправляет результирующую колекцию в фильтр</li>
     * <li>результат фильтрации выдается во вне</li>
     * </ul>
     *
     * @param region регион для поиска
     * @param filter фильтр объектов
     * @return соллекция содержащая объекты соответствующие критериям фильтра
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    @SuppressWarnings({"unchecked"})
    Collection searchObjects(Object region, StorageFilter filter) throws TimeoutException {

        // контейнер для агрегации данных из основного и временных хранилищь
        /*
        final Collection summary = new HashSet();

        if (!deletedRegions.contains(region) && !ignoredRegionsContent.contains(region)) {
            summary.addAll(Cache.getRegionContent(region));
            summary.addAll(localCache.getRegionContent(region).values());

            if (deletedObjects.containsKey(region)) {
                for (Object o : deletedObjects.get(region)) {
                    summary.remove(Cache.get(region, o));
                }
            }
        }
        */
        Collection exclusions = new ArrayList();
        if (deletedObjects.containsKey(region)) {
            for (Object o : deletedObjects.get(region)) {
                exclusions.add(Cache.get(region, o));
            }
        }

        return filter.filter(Cache.getIndexesForRegion(region),
                Cache.getRegionContent(region), localCache.getRegionContent(region).values(), exclusions);
    }

    /**
     * Производит поиск значений в хранилище по переданным критериям содержащимся в {@link StorageFilter}
     * <p/>
     * Последовательность работы:
     * <ul>
     * <li>проверяет списки удаленных регионов,<br>
     * если переданный регион не в списках - идет дальше, иначе - вернется пустая колекция</li>
     * <li>проверяет списки зачищенных регионов,<br>
     * если переданный регион не в списках - идет дальше, иначе - вернется пустая колекция</li>
     * <li>получает содержимое основного и сессионного кешей, суммирует их</li>
     * <li>из полученного резльтата вычитает то что в сессии помечено на удаление</li>
     * <li>отправляет результирующую колекцию в фильтр</li>
     * <li>результат фильтрации выдается во вне</li>
     * </ul>
     *
     * @param filters фильтры объектов
     * @return соллекция содержащая объекты соответствующие критериям фильтра
     * @throws TimeoutException при превышении времени ожидания постановки блокировки
     */
    Collection searchObjects(Collection<StorageFilter> filters) throws TimeoutException {

        Collection extractedValues = null;
        Collection filterResult = null;

        for (StorageFilter filter : filters) {

            if (extractedValues != null) {
                filter.prepareCriterionValues(extractedValues);
            }

            filterResult = searchObjects(filter.getRegionKey(), filter);
            extractedValues = filter.extractValues(filterResult);
        }

        if (extractedValues != null && !extractedValues.isEmpty()) {
            filterResult = extractedValues;
        }

        return filterResult;
    }

    public boolean containsObjects(Object region, StorageFilter filter) throws TimeoutException {
        filter.setFirstOnly(true);
        return !searchObjects(region, filter).isEmpty();
    }

    public boolean containsObjects(Collection<StorageFilter> filters) throws TimeoutException {

        StorageFilter last = null;
        for (StorageFilter filter : filters) {
            last = filter;
        }

        if (last != null) {
            last.setFirstOnly(true);
        }

        return !searchObjects(filters).isEmpty();
    }

    public int getObjectCount(Object region, StorageFilter filter) throws TimeoutException {
        return searchObjects(region, filter).size();
    }

    public int getObjectCount(Collection<StorageFilter> filters) throws TimeoutException {
        return searchObjects(filters).size();
    }

    Set<Object> getKeys(Object region) throws RegionNotFoundException {

        final Set<Object> result = new HashSet<>();

        if (localCache.hasRegion(region)) {
            result.addAll(localCache.getKeys(region));
        }

        if (Cache.hasRegion(region)) {
            final Set<Object> set = new HashSet<>(Cache.getKeys(region));
            set.removeAll(deletedRegions);
            result.addAll(Cache.getKeys(region));
        }

        return Collections.unmodifiableSet(result);
    }

    boolean hasRegion(Object region) {

        boolean result = false;

        if (!deletedRegions.contains(region)) {
            result = Cache.hasRegion(region);

            if (!result) {
                result = localCache.hasRegion(region);
            }
        }

        return result;
    }

    Set<Object> getRegions() {

        final Set<Object> result = localCache.getRegions();
        result.addAll(Cache.getRegions());
        result.removeAll(deletedRegions);

        return result;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Добавляет команду в последовательность сессии
     *
     * @param action команда для накатывания на кеш
     * @throws IllegalStateException при попытке добавления на замороженом конекшене
     */
    private void addAction(Action action) throws IllegalStateException {
        if (freeze) {
            throw new IllegalStateException();
        }
        actions.add(action);
    }

    /**
     * Создает добавления объекта в кеш и добавляет её в последовательность сессии
     *
     * @param region регион
     * @param key    ключ
     * @param val    значение
     * @throws IllegalStateException при попытке добавления на замороженом конекшене
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    void addPutAction(Object region, Object key, Object val) throws TimeoutException, IllegalStateException {
        addAction(new PutAction(region, key, val));
    }

    /**
     * Создает команду добавления объекта в кеш и добавляет её в последовательность сессии
     *
     * @param region регион
     * @param values значение
     * @throws IllegalStateException при попытке добавления на замороженом конекшене
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    void addPutAction(Object region, Map values) throws TimeoutException, IllegalStateException {
        addAction(new PutMapAction(region, values));
    }

    /**
     * Создает команду удаления объекта из кеша и добавляет её в последовательность сессии
     *
     * @param region регион
     * @param key    ключ
     * @throws IllegalStateException при попытке работы на замороженом конекшене
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    void addRemoveAction(Object region, Object key) throws TimeoutException, IllegalStateException {
        addAction(new RemoveAction(region, key));
    }

    /**
     * Создает команду создания региона кеша и добавляет её в последовательность сессии
     *
     * @param region регион
     * @throws IllegalStateException при попытке работы на замороженом конекшене
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    void addCreateRegionAction(Object region) throws TimeoutException, IllegalStateException {
        addAction(new CreateRegionAction(region));
    }

    /**
     * Создает команду очистки региона кеша и добавляет её в последовательность сессии
     *
     * @param region регион
     * @throws IllegalStateException при попытке работы на замороженом конекшене
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    void addClearRegionAction(Object region) throws TimeoutException, IllegalStateException {
        addAction(new ClearRegionAction(region));
    }

    /**
     * Создает команду удаления региона кеша и добавляет её в последовательность сессии
     *
     * @param region регион
     * @throws IllegalStateException при попытке работы на замороженом конекшене
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    void addRemoveRegionAction(Object region) throws TimeoutException, IllegalStateException {
        addAction(new RemoveRegionAction(region));
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Реализация патерна Command
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /** Общий класс для реализации патерна Command */
    abstract class Action {

        /** Состояние команды, применена или нет */
        protected boolean applied = false;

        /** Состояние команды, откат состоялся или нет */
        protected boolean rolledBack = false;

        /** Реализация метода должна содержать код применения команды на кеше */
        public abstract void execute();

        /** Реализация метода должна содержать код отката команды на кеше */
        public abstract void rollback();

        /** Действия выполняемые при завершении взаимодействия в рамках сессии */
        protected abstract void clear();

        /** Общий код выполняемый перед применением */
        protected void beforeExec() {
            if (applied) {
                throw new IllegalStateException("Action already applied");
            }
        }

        /** Общий код выполняемый после применения */
        protected void afterExec() {
            applied = true;
        }

        /** Общий код выполняемый перед откатом */
        protected void beforeRollback() {
            if (!applied || rolledBack) {
                throw new IllegalStateException("Cant rollback in current state: applied["
                        + applied + "];  rolledBack[" + rolledBack + "]");
            }
        }

        /** Общий код выполняемый после отката */
        protected void afterRollback() {
            rolledBack = true;
        }

        protected void lock(Object region, Object key) throws TimeoutException {
            DefaultConnection.this.lock(this.getClass().getSimpleName(), region, key);
        }

        protected void unLock(Object region, Object key) {
            DefaultConnection.this.unLock(region, key);
        }

    } // abstract Action

    /** Команда удаления региона */
    private class RemoveRegionAction extends Action {

        /** Идентификатор удаляемого ргиона */
        private final Object region;

        /** Экшен очистки региона, используется для хранения данных на откат транзакции */
        private final ClearRegionAction clearRegionAction;

        /**
         * Конструирует команду удаления региона
         *
         * @param region удаляемый регион
         * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
         */
        RemoveRegionAction(Object region) throws TimeoutException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE,
                        "Creating " + this.getClass().getSimpleName() + ", key: " + region + ". On " + getId());
            }

            this.region = region;
            clearRegionAction = new ClearRegionAction(region);
            deletedRegions.add(this.region);
        }

        @Override
        public void execute() {
            beforeExec();
            clearRegionAction.execute();
            Cache.removeRegion(region);
            afterExec();
        }

        @Override
        public void rollback() {
            beforeRollback();
            Cache.createRegion(region);
            clearRegionAction.rollback();
            afterRollback();
        }

        @Override
        protected void clear() {
            clearRegionAction.clear();
        }
    }

    /** Команда зачистки региона */
    private class ClearRegionAction extends Action {

        /** Идентификатор зачищаемого ргиона */
        private final Object region;

        /** Временное хранилище данных зачищаемого региона */
        private final HashMap<Object, Object> map = new HashMap<>();

        /**
         * Конструирует команду
         *
         * @param region зачищаемый регион
         * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
         */
        ClearRegionAction(Object region) throws TimeoutException {
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE,
                        "Creating " + this.getClass().getSimpleName() + ", key: " + region + ". On " + getId());
            }

            this.region = region;
            lock(this.region, this.region);
            ignoredRegionsContent.add(this.region);
        }

        @Override
        public void execute() {

            beforeExec();

            final Set<Object> keys = new HashSet<>(Cache.getKeys(region));
            for (Object key : keys) {
                map.put(key, Cache.get(region, key));
            }

            Cache.clearRegion(region);
            afterExec();
        }

        @Override
        public void rollback() {

            beforeRollback();

            for (Map.Entry<Object, Object> entry : map.entrySet()) {
                Cache.put(region, entry.getKey(), entry.getValue());
            }

            afterRollback();
        }

        @Override
        protected void clear() {
            unLock(this.region, this.region);
        }

    } // ClearRegionAction

    /** Команда создания региона */
    private class CreateRegionAction extends Action {

        /** Идентификатор создаваемого ргиона */
        private final Object region;

        /**
         * Конструирует команду
         *
         * @param region создаваемый регион
         * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
         */
        CreateRegionAction(Object region) throws TimeoutException {
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE,
                        "Creating " + this.getClass().getSimpleName() + ", key: " + region + ". On " + getId());
            }

            this.region = region;
            lock(this.region, this.region);
            localCache.createRegion(region);
        }

        @Override
        public void execute() {
            beforeExec();
            Cache.createRegion(region);
            afterExec();
        }

        @Override
        public void rollback() {
            beforeRollback();
            Cache.removeRegion(region);
            afterRollback();
        }

        @Override
        protected void clear() {
            unLock(this.region, this.region);
        }

    } // end of CreateRegion


    /** Команда размещения объекта в кеше */
    private class PutAction extends Action {

        /** Идентификатор ргиона объекта */
        private final Object region;
        /** Ключ объекта */
        private final Object key;

        /** Размещаемое значение */
        private final Object val;

        /**
         * Конструирует команду
         *
         * @param region регион для размещения
         * @param key    ключ объекта
         * @param val    размещаемый объект
         * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
         */
        PutAction(Object region, Object key, Object val) throws TimeoutException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Creating " + this.getClass().getSimpleName()
                        + ", key: " + key + "; val: " + val + ". On " + getId());
            }

            this.region = region;
            this.key = key;
            this.val = val;

            lock(this.region, this.key);

            if (!localCache.hasRegion(this.region)) {
                localCache.createRegion(this.region);
            }

            localCache.put(this.region, this.key, this.val);
        }

        @Override
        public void execute() {
            beforeExec();
            Cache.put(region, key, val);
            afterExec();
        }

        @Override
        public void rollback() {
            beforeRollback();
            Cache.remove(region, key);
            afterRollback();
        }

        @Override
        protected void clear() {
            unLock(this.region, this.key);
        }

    } // end of PutAction


    /** Команда массового размещения объектов в кеше */
    private class PutMapAction extends Action {

        /** Регион для размещения */
        private final Object region;

        /** Размещаемые значения */
        private final Map values;


        /**
         * Конструирует команду
         *
         * @param region регион для размещения
         * @param values размещаемый объект
         * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
         */
        PutMapAction(Object region, Map values) throws TimeoutException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Creating " + this.getClass().getSimpleName()
                        + ", Region: " + region + "; vals: " + values + ". On " + getId());
            }

            this.region = region;
            this.values = values;

            lock(this.region, this.region);

            if (!localCache.hasRegion(this.region)) {
                localCache.createRegion(this.region);
            }

            localCache.putAll(this.region, this.values);
        }

        @Override
        public void execute() {
            beforeExec();
            Cache.put(region, values);
            afterExec();
        }

        @Override
        public void rollback() {

            beforeRollback();

            for (Object o : values.keySet()) {
                Cache.remove(region, o);
            }

            afterRollback();
        }

        @Override
        protected void clear() {
            unLock(this.region, this.region);
        }

    } // end of PutMapAction

    /** Команда удаления объекта из кеша */
    private class RemoveAction extends Action {

        /** Регион удаляемого объекта */
        private final Object region;

        /** Ключ удаляемого объекта */
        private final Object key;

        /** Удаляемый объект */
        private Object val;


        /**
         * Конструирует команду
         *
         * @param region регион для поиска
         * @param key    ключ удаляемого объекта
         * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
         */
        RemoveAction(Object region, Object key) throws TimeoutException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Creating {0}, key[{1}], Session[{2}] ",
                        new Object[]{this.getClass().getSimpleName(), key, getId()});
            }

            this.region = region;
            this.key = key;

            lock(this.region, this.key);
            if (localCache.hasRegion(this.region)) {
                localCache.remove(this.region, this.key);
            }

            deletedObjects.computeIfAbsent(this.region, k -> new HashSet<>());

            deletedObjects.get(this.region).add(this.key);
        }

        @Override
        public void execute() {
            beforeExec();
            val = Cache.get(region, key);
            Cache.remove(region, key);
            afterExec();
        }

        @Override
        public void rollback() {
            beforeRollback();
            Cache.put(region, key, val);
            afterRollback();
        }

        @Override
        protected void clear() {
            unLock(this.region, this.key);
        }

    } // end of RemoveAction

}
