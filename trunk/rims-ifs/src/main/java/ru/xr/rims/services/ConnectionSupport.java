/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

import ru.xr.cache.RegionNotFoundException;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Интерфейс определяет набор методов для работы с RMS, в рамках сессии.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConnectionSupport.java 76 2011-04-26 12:36:13Z dgomon $"
 */
public interface ConnectionSupport extends Remote, Serializable {

    /**
     * Возвращает уникальный идентификатор сессии.
     * <p/>
     * Используется для разделения вносимых изменений на транзакции
     *
     * @return идентификатор сессии
     * @throws java.rmi.RemoteException при ошибке получения идентификатора
     */
    Object createConnection() throws RemoteException;

    /**
     * Размещает значение в указанном регионе, под указанным ключем
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @param key          ключ для размещения
     * @param val          размещаемое значение
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void put(Object connectionId, Object region, Object key, Object val)
            throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Размещает значения в указанном регионе
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @param values       значения
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void put(Object connectionId, Object region, Map values)
            throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Размещает значения, через массив байт, в указанном регионе
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @param values       массив байт (карта)
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    public void putMap(Object connectionId, Object region, byte[] values)
            throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Получает значение в указанном регионе, под указанным ключем
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @param key          ключ для поиска
     * @return значение хранимое в кеше
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Object get(Object connectionId, Object region, Object key) throws RemoteException, RegionNotFoundException;

    /**
     * Получает все значения из региона
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @return значение хранимое в кеше
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Map getRegionContent(Object connectionId, Object region) throws RemoteException, RegionNotFoundException;

    /**
     * Получает все значения из региона (карта) в виде сассива байт
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @return значение хранимое в кеше
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    byte[] getRegionContentAsBytes(Object connectionId, Object region) throws RemoteException, RegionNotFoundException;

    /**
     * Удаляет значение в указанном регионе, под указанным ключем
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @param key          ключ для поиска
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void remove(Object connectionId, Object region, Object key) throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Получает набор ключей в указанном регионе
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @return набор значений
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Set<Object> getKeys(Object connectionId, Object region) throws RemoteException, RegionNotFoundException;

    /**
     * Возвращает признак наличия региона
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @return true - регион существуует, иначе - false
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    boolean hasRegion(Object connectionId, Object region) throws RemoteException;

    /**
     * Создает регион с указанным идентификатором
     *
     * @param connectionId идентификатор сессии
     * @param region       идентификатор создаваемого региона
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void createRegion(Object connectionId, Object region) throws RemoteException, TimeoutException;

    /**
     * Зачищает регион с указанным идентификатором
     *
     * @param connectionId идентификатор сессии
     * @param region       идентификатор создаваемого региона
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void clearRegion(Object connectionId, Object region) throws RemoteException, TimeoutException;

    /**
     * Возвращает набор идентификаторов доступных регионов
     *
     * @param connectionId идентификатор сессии
     * @return перечисление идентификаторов регионов
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Set getRegions(Object connectionId) throws RemoteException;

    /**
     * Удаляет регион с указанным идентификатором
     *
     * @param connectionId идентификатор сессии
     * @param region       идентификатор создаваемого региона
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void removeRegion(Object connectionId, Object region) throws RemoteException, TimeoutException;

    /**
     * Производит поиск значений в хранилище по переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @param filter       фильтр объектов
     * @return коллекция содержащая объекты соответствующие критериям фильтра
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    Collection searchObjects(Object connectionId, Object region, StorageFilter filter) throws RemoteException;

    /**
     * Производит поиск значений в хранилище по переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param connectionId идентификатор сессии
     * @param filters      фильтры объектов
     * @return коллекция содержащая объекты соответствующие критериям фильтра
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    Collection searchObjects(Object connectionId, Collection<StorageFilter> filters) throws RemoteException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @param filter       фильтр объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    boolean containsObjects(Object connectionId, Object region, StorageFilter filter) throws RemoteException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param connectionId идентификатор сессии
     * @param filters      фильтры объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    boolean containsObjects(Object connectionId, Collection<StorageFilter> filters) throws RemoteException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для поиска
     * @param filter       фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    int getObjectCount(Object connectionId, Object region, StorageFilter filter) throws RemoteException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param connectionId идентификатор сессии
     * @param filters      фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    int getObjectCount(Object connectionId, Collection<StorageFilter> filters) throws RemoteException;

    /**
     * Устанавливает блокировку на объекте размещенном в указанном регионе, под указанным ключем
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @param key          ключ для размещения
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void lock(Object connectionId, Object region, Object key) throws RemoteException, TimeoutException;


    /**
     * Устанавливает блокировку на объекте размещенном в указанном регионе, под указанным ключем
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @param key          ключ для размещения
     * @param timeOut      максимальное время (в мс) ожидания постановки блокировки
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void lock(Object connectionId, Object region, Object key, long timeOut) throws RemoteException, TimeoutException;

    /**
     * Снимает блокировку с объекта размещенного в указанном регионе, под указанным ключем
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @param key          ключ для размещения
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    void unlock(Object connectionId, Object region, Object key) throws RemoteException;

    /**
     * Снимает блокировку с объекта размещенного в указанном регионе, под указанным ключем
     *
     * @param connectionId идентификатор сессии
     * @param region       регион для размещения
     * @param key          ключ для размещения
     * @param timeOut      максимальное время (в мс) ожидания постановки блокировки
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    void unLock(Object connectionId, Object region, Object key, long timeOut) throws RemoteException;

    /**
     * Применяет выполненные на сессии действия к основному кешу
     *
     * @param connectionId идентификатор сессии
     * @throws Exception при возникновении ошибок в процессе применения изменений
     */
    void commit(Object connectionId) throws Exception;

    /**
     * Отменяет выполненные на сессии действия и сбрасывает состояние сессии на стороне кеша
     *
     * @param connectionId идентификатор сессии
     * @throws Exception при возникновении ошибок в процессе отката изменений
     */
    void rollback(Object connectionId) throws Exception;

    /**
     * Закрывает текущую ссессию
     *
     * @param connectionId идентификатор сессии
     * @throws Exception при возникновении ошибок при обращении к кешу
     */
    void close(Object connectionId) throws Exception;


}
