/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

/**
 * Собирательный интерфейс, возможно, в дальнейшем будет упразнен
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: GlobalService.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public interface GlobalService extends RemoteStorage, ConnectionSupport, EchoSupport {
}
