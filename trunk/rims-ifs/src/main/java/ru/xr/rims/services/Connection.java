/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

import java.rmi.Remote;

/**
 * Интерфейс определяет методы для управления сессией с RMS
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Connection.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public interface Connection extends Remote {

    /**
     * Применяет внесенные, в рамках сессии, изменения на основной кеш
     *
     * @throws Exception в случае невозможности применения
     */
    void commit() throws Exception;

    /** Откатывает внесенные, в рамках сессии, изменения на основной кеш */
    void rollback();

    /** Закрывает текущую сессию */
    void close();
}
