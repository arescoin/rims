/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

/**
 * Сигнализирует о превышении допустимого времени ожидания постановки блокировки
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TimeoutException.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public class TimeoutException extends Exception {

    public TimeoutException() {
        super();
    }

    public TimeoutException(String message) {
        super(message);
    }
}
