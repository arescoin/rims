/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Определяет методы для выполнения удаленного поиска на стороне сервера кеша
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: StorageFilter.java 136 2011-10-27 11:00:40Z dgomon $"
 */
public interface StorageFilter extends Serializable {

    /**
     * Выполняет поиск
     *
     * @param source исходные объекты
     * @return результат работы фильтра
     */
    Collection filter(Collection source);

    /**
     * Выполяет поиск
     *
     * @param indexes    карта существующих индексов
     * @param source     исходные объекты
     * @param temporary  исходные объекты из временного хранилища
     * @param exclusions исключения из результатов поиска
     * @return результат работы фильтра
     */
    Collection filter(Map<String, Map<Object, List<Object>>> indexes,
            Collection source, Collection temporary, Collection exclusions);

    /**
     * Возвращает ключ региона содержащего данные для поиска
     *
     * @return ключ региона
     */
    Object getRegionKey();

    /**
     * Устанавливает значения для критерия поиска по результатам предыдущего
     *
     * @param collection результаты предыдущего поиска
     */
    void prepareCriterionValues(Collection collection);

    /**
     * Определяет необходимость обработки отфильтрованной коллекции.
     * Необходим для простого поиска по одному фильтру.
     *
     * @return <code>true</code> - если нужно извлечь данные из отфильтрованной коллекции,
     *         <code>false</code> иначе.
     */
    boolean isExtractionRequiered();

    /**
     * Обрабатывает коллекцию данных, возвращая коллекцию данных для следующего поиска
     *
     * @param collection коллекция данных для подлежащих обработке
     * @return данные экстрагированные из входной коллекции
     */
    Collection extractValues(Collection collection);

    /**
     * Устанавливает признак остановки фильтрации при обнаружении первого элемента
     *
     * @param firstOnly переключатель true - остановиться на первом, иначе fale
     */
    void setFirstOnly(boolean firstOnly);
}
