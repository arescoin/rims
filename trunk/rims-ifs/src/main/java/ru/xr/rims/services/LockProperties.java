/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

import java.io.Serializable;

/**
 * Интерфейс контейнера для работы с блокировками на кеше
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: LockProperties.java 87 2011-06-06 15:39:29Z dgomon $"
 */
public interface LockProperties extends Serializable {

    /**
     * Возвращает идентификатор сессии
     *
     * @return идентификатор сессии
     */
    Object getSessionId();

    /**
     * Возвращает ключ для блокировки
     *
     * @return ключ
     */
    Object getKey();
}
