/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

import ru.xr.cache.RegionNotFoundException;

import java.io.*;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Интерфейс описывает набор сервисов предоставляемых сервером RiMS доступных для клиентской части
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RemoteStorage.java 76 2011-04-26 12:36:13Z dgomon $"
 */
public interface RemoteStorage extends Remote, Serializable {

    /** Имя в JNDI */
    static final String REMOTE_ELEMENT = "REMOTE_CACHE";

    /**
     * Размещает значение в указанном регионе, под указанным ключем
     *
     * @param region регион для размещения
     * @param key    ключ для размещения
     * @param val    размещаемое значение
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void put(Object region, Object key, Object val) throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Размещает значения в указанном регионе
     *
     * @param region регион для размещения
     * @param values размещаемые значения
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void put(Object region, Map values) throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Размещает значения, через массив байт, в указанном регионе
     *
     * @param region регион для размещения
     * @param values массив байт (карта)
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    public void putMap(Object region, byte[] values)
            throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Получает значение в указанном регионе, под указанным ключем
     *
     * @param region регион для поиска
     * @param key    ключ для поиска
     * @return значение хранимое в кеше
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Object get(Object region, Object key) throws RemoteException, RegionNotFoundException;

    /**
     * Получает все значения из региона
     *
     * @param region регион для поиска
     * @return значение хранимое в кеше
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Map getRegionContent(Object region) throws RemoteException, RegionNotFoundException;

    /**
     * Получает все значения из региона (карта) в виде массива байт
     *
     * @param region регион для поиска
     * @return значение хранимое в кеше
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    byte[] getRegionContentAsBytes(Object region) throws RemoteException, RegionNotFoundException;

    /**
     * Удаляет значение в указанном регионе, под указанным ключем
     *
     * @param region регион для поиска
     * @param key    ключ для поиска
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void remove(Object region, Object key) throws RemoteException, RegionNotFoundException, TimeoutException;

    /**
     * Получает набор ключей в указанном регионе
     *
     * @param region регион для поиска
     * @return набор значений
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Set<Object> getKeys(Object region) throws RemoteException, RegionNotFoundException;

    /**
     * Возвращает признак наличия региона
     *
     * @param region регион для размещения
     * @return true - регион существуует, иначе - false
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    boolean hasRegion(Object region) throws RemoteException;

    /**
     * Создает регион с указанным идентификатором
     *
     * @param region идентификатор создаваемого региона
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void createRegion(Object region) throws RemoteException, TimeoutException;

    /**
     * Зачищает регион с указанным идентификатором
     *
     * @param region идентификатор создаваемого региона
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void clearRegion(Object region) throws RemoteException, TimeoutException;


    /**
     * Возвращает набор идентификаторов доступных регионов
     *
     * @return перечисление идентификаторов регионов
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    Set getRegions() throws RemoteException;

    /**
     * Удаляет регион с указанным идентификатором
     *
     * @param region идентификатор создаваемого региона
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void removeRegion(Object region) throws RemoteException, TimeoutException;

    /**
     * Устанавливает блокировку на объекте размещенном в указанном регионе, под указанным ключем
     *
     * @param region регион для размещения
     * @param key    ключ для размещения
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     * @throws TimeoutException при привышении времени ожидания очереди
     */
    void lock(Object region, Object key) throws RemoteException, TimeoutException;


    /**
     * Снимает блокировку с объекта размещенного в указанном регионе, под указанным ключем
     *
     * @param region регион для размещения
     * @param key    ключ для размещения
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     * @throws ru.xr.cache.RegionNotFoundException при попытке использования несуществующего региона
     */
    void unlock(Object region, Object key) throws RemoteException;

    /**
     * Производит поиск значений в хранилище по переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param region регион для поиска
     * @param filter фильтр объектов
     * @return коллекция содержащая объекты соответствующие критериям фильтра
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    Collection searchObjects(Object region, StorageFilter filter) throws RemoteException;

    /**
     * Производит поиск значений в хранилище по переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return коллекция содержащая объекты соответствующие критериям фильтра
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    Collection searchObjects(Collection<StorageFilter> filters) throws RemoteException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param region регион для поиска
     * @param filter фильтр объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    boolean containsObjects(Object region, StorageFilter filter) throws RemoteException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    boolean containsObjects(Collection<StorageFilter> filters) throws RemoteException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param region регион для поиска
     * @param filter фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    int getObjectCount(Object region, StorageFilter filter) throws RemoteException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    int getObjectCount(Collection<StorageFilter> filters) throws RemoteException;

    /** Утилитный класс для конвертации объект - байт - объект */
    static class ByteConverter {

        public static byte[] objectToBytes(Object object) throws RemoteException {
            byte[] resultBytes;
            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(object);
                resultBytes = outputStream.toByteArray();

                objectOutputStream.flush();
                objectOutputStream.close();

                outputStream.flush();
                outputStream.close();

                return resultBytes;

            } catch (IOException e) {
                throw new RemoteException("Error while object preparation", e);
            }
        }

        public static Object bytesToObject(byte[] values) throws RemoteException {

            try {

                ByteArrayInputStream byteInputStream = new ByteArrayInputStream(values);
                ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);

                return objectInputStream.readObject();
            } catch (IOException e) {
                throw new RemoteException("Can't read object", e);
            } catch (ClassNotFoundException e) {
                throw new RemoteException("Can't read object", e);
            }
        }
    }
}
