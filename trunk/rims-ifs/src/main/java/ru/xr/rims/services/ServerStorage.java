/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

import ru.xr.cache.RegionNotFoundException;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.Set;

/**
 * Интерфейс описывает набор сервисов предоставляемых сервером RiMS доступных для административной части
 * <p/>
 * По сути, в основном, дублирует {@link ru.xr.rims.services.RemoteStorage}, за исключением методов:
 * <p/>
 * <ul>
 * <li>{@link #ping()}</li>
 * <li>{@link #runGC()}</li>
 * <li>{@link #getLocks()}</li>
 * <li>{@link #getLocksForRegion(Object)}</li>
 * <li>{@link #killLock(Object, Object, Object)}</li>
 * <li>{@link #stopRequest()}</li>
 * </ul>
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ServerStorage.java 72 2011-04-18 13:14:48Z dgomon $"
 */
public interface ServerStorage extends Remote {

    /** Имя в JNDI */
    String REMOTE_ELEMENT = "SERVER_CACHE";

    /** Стандартный ответ для метода {@link #ping()} */
    String PING = "pong";

    void put(Object region, Object key, Object val) throws RemoteException, RegionNotFoundException;

    void put(Object region, Map values) throws RemoteException, RegionNotFoundException;

    Object get(Object region, Object key) throws RemoteException, RegionNotFoundException;

    Map getRegionContent(Object region) throws RemoteException, RegionNotFoundException;

    void remove(Object region, Object key) throws RemoteException, RegionNotFoundException;

    Set<Object> getKeys(Object region) throws RemoteException, RegionNotFoundException;

    boolean hasRegion(Object region) throws RemoteException;

    void createRegion(Object region) throws RemoteException;

    void clearRegion(Object region) throws RemoteException;

    Set getRegions() throws RemoteException;

    void removeRegion(Object region) throws RemoteException;

    void lock(Object region, Object key) throws RemoteException, TimeoutException;

    void unlock(Object region, Object key) throws RemoteException;

    /**
     * Возвращает стандартный ответ содержащий значение константы {@link #PING}
     *
     * @return значение константы {@link #PING}
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    String ping() throws RemoteException;

    /**
     * Инициирует принудительный запуск GC на стороне сервера
     *
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    void runGC() throws RemoteException;

    /**
     * Возвращает карту блокировок установленных на кеше.
     *
     * @return Ключ - идентификатор региона, значение набор блокированных ключей в регионе
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    Map<Object, Set<LockProperties>> getLocks() throws RemoteException;

    /**
     * Возвращает набор идентификаторов находящихся в блокировке в конкретном регионе кеша
     *
     * @param region идентификатор региона
     * @return набор ключей
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    Set<LockProperties> getLocksForRegion(Object region) throws RemoteException;

    /**
     * Принудительно снимает блокировку (только в рамках сессии)
     *
     * @param connectionId идентификатор сессии
     * @param region       идентификатор региона
     * @param key          ключ блокировки
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    void killLock(Object connectionId, Object region, Object key) throws RemoteException;

    /**
     * Отправляет запрос на остановку сервера, попросу говоря - прибивает его
     *
     * @throws java.rmi.RemoteException при возникновении ошибки на транспортном уровне
     */
    void stopRequest() throws RemoteException;
}
