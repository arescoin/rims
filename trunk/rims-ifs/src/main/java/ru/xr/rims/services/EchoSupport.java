/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.services;

import java.io.Serializable;
import java.rmi.Remote;

/**
 * Реализация позволяет получить простой эхо-сервис
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: EchoSupport.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public interface EchoSupport extends Remote, Serializable {

    /**
     * Возвращает в виде ответа полученное сообщение
     * <p/>
     * В целях предотвращения попыток нецелевого использования, максимально допустимая длина строки - 100 символов
     *
     * @param message исходное сообщение
     * @return ответ
     * @throws Exception при некорректном сообщении
     */
    String echo(String message) throws Exception;
}
