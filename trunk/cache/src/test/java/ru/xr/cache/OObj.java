/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.cache;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: OObj.java 165 2014-07-10 10:57:16Z DGomon $"
 */
public class OObj {
    private String val;

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
