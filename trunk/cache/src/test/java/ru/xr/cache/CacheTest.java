/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.cache;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;


/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CacheTest.java 164 2014-07-10 10:50:23Z DGomon $"
 */
public class CacheTest {

    private static final String REGION_NAME = "TestRegion_1";
    private static final String KEY_NAME = "TestRegion_1_Key";
//    private static final String OBJ_VAL = "TestRegion_1_Key_Val";
    private static final OObj O_OBJ = new OObj();

    @Test
    public void testRegion() {
        if (Cache.hasRegion(REGION_NAME)) {
            Cache.removeRegion(REGION_NAME);
        }

        O_OBJ.setVal("TST_VAL");

        Assert.assertEquals("Invalid Region size", 0, Cache.getRegions().size());

        Cache.createRegion(REGION_NAME);
        Assert.assertTrue("Region not found: " + REGION_NAME, Cache.hasRegion(REGION_NAME));
        Assert.assertEquals("Invalid Region size", 1, Cache.getRegions().size());

        HashMap hashMap = new HashMap();
        hashMap.put(KEY_NAME, O_OBJ);

        Cache.put(REGION_NAME, hashMap);
        Cache.put(REGION_NAME, KEY_NAME, O_OBJ);
        Assert.assertEquals("Invalid Region size", 1, Cache.getKeys(REGION_NAME).size());

        Cache.clearRegion(REGION_NAME);
        Assert.assertEquals("Invalid Region size", 0, Cache.getKeys(REGION_NAME).size());

        Cache.removeRegion(REGION_NAME);
        Assert.assertTrue("Region must be deleted: " + REGION_NAME, !Cache.hasRegion(REGION_NAME));

        Cache.put(REGION_NAME, KEY_NAME, O_OBJ);
        Cache.remove(REGION_NAME, KEY_NAME);
        Assert.assertNull("Value must be deleted", Cache.get(REGION_NAME, KEY_NAME));

    }

//    @Test(expected = RegionNotFoundException.class)
//    public void regionFailure() {
//        if (Cache.hasRegion(REGION_NAME)) {
//            Cache.removeRegion(REGION_NAME);
//        }
//
//        Cache.getKeys(REGION_NAME);
//    }

    @Test
    public void testKey() {
        if (Cache.hasRegion(REGION_NAME)) {
            Cache.removeRegion(REGION_NAME);
        }

        Cache.createRegion(REGION_NAME);

        Cache.put(REGION_NAME, KEY_NAME, O_OBJ);
        Assert.assertEquals(O_OBJ, Cache.get(REGION_NAME, KEY_NAME));

        Cache.remove(REGION_NAME, KEY_NAME);
        Assert.assertNull(Cache.get(REGION_NAME, KEY_NAME));
    }

    protected void fail(Exception e) throws AssertionError {
        AssertionError ae = new AssertionError(e.getMessage());
        ae.initCause(e);
        throw ae;
    }
}
