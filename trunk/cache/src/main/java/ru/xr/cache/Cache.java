/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.cache;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Хранилище данных. Организовано в виде карты, содержащей, в свою очередь, карты.
 * <p/>
 * Верхний уровень - регионы<br/>
 * Вложенный - значения
 * <p/>
 * Доступ к данным осуществляется с указанием ключа региона и ключа объекта.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Cache.java 156 2011-12-28 10:36:25Z dgomon $"
 */
public class Cache {

    private static final String INDEX_PATH_KEY = "ru.xr.cache.Cache_INDEX_PATH_KEY";

    private static final Cache INSTANCE = new Cache();

    /** Карта регионов */
    private final Map<Object, Map<Object, Object>> cache = new ConcurrentHashMap<>();

    /**
     * Карта индексов:
     * <pre>
     *  Регион : Карта
     *             |
     *           Имя атрибута : Карта
     *                            |
     *                          Значение атрибута : Набор&LTОбект (владелец атрибута)&GT
     * </pre>
     * <p/>
     * Другими словами: Map<Регион, Map<Имя атрибута, Map<Значение атрибута, Set<Обект (владелец атрибута)>>>>
     */
    private final Map<Object, Map<String, Map<Object, List<Object>>>> indexes =
            new ConcurrentHashMap<>();

    /**
     * Карта процессоров индексов:
     * <pre>
     *  Тип объекта : Карта
     *                  |
     *               Имя атрибута : Процессор
     * </pre>
     * <p/>
     * Другими словами: Map<Тип объекта, Map<Имя атрибута, Процессор>>
     */
    private final Map<Class, Map<String, ValueProcessor>> processors =
            new ConcurrentHashMap<>();


    private Cache() {
        for (File indexFile : getIndexFiles()) {
            readIndex(indexFile);
        }
    }

    private ArrayList<File> getIndexFiles() {
        final String indexPathString = System.getProperty(INDEX_PATH_KEY, "index");
        final File indexDir = new File(indexPathString);
        final ArrayList<File> indexFiles = new ArrayList<>();

        if (indexDir.exists() && indexDir.isDirectory() && indexDir.canRead()) {
            System.out.println("Try to read indexes from [" + indexDir.getAbsolutePath() + "]");

            String[] childFiles = indexDir.list();

            if (childFiles != null) {

                for (String indexFile : childFiles) {
                    File concreteIndexFile = new File(indexPathString + "/" + indexFile);
                    if (concreteIndexFile.isFile()) {
                        indexFiles.add(concreteIndexFile);
                    }
                }

                System.out.println("Has [" + indexFiles.size()
                        + "] index files in [" + indexDir.getAbsolutePath() + "]");
            }
        }

        return indexFiles;
    }

    private void readIndex(File file) {
        try {
            Properties properties = new Properties();
            properties.load(new FileReader(file));

            Set set = properties.keySet();

            System.out.println(set);

            for (Object key : set) {

                HashMap<String, Map<Object, List<Object>>> indexMap = new HashMap<>();
//                Map<Object, ValueProcessor> processorsMap = new HashMap<Object, ValueProcessor>();
                String value = properties.getProperty((String) key);
                String[] fields = value.split(",");

                for (String field : fields) {
                    String concreteKey = field.trim();
                    indexMap.put(concreteKey, null);
                }

                indexes.put(key, indexMap);
//                processors.put(key, processorsMap);
            }

        } catch (IOException e) {
            throw new RuntimeException("Can't parse index config", e);
        }
    }

    /**
     * Размещает объект в указанном регионе по указанному ключу,
     * в случае присутствия там объекта, он будет заменен на новый
     *
     * @param region регион для размещения
     * @param key    ключ для размещения
     * @param val    размещаемое значение
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    public static void put(Object region, Object key, Object val) throws RegionNotFoundException {
        checkRegion(region);

        if (INSTANCE.indexes.containsKey(region)) {
            processPut(region, key, val);
        }

        INSTANCE.cache.get(region).put(key, val);
    }

    private static void processPut(Object region, Object key, Object val) {

        if (!INSTANCE.indexes.containsKey(region)) {
            return;
        }

        try {
            checkProcessor(region, val);

            final Object oldVal = INSTANCE.cache.get(region).get(key);

            if (oldVal != null) {
                cleanOldValue(region, oldVal);
            }

            final Set<Map.Entry<String, Map<Object, List<Object>>>> attrNameToValMapSet =
                    INSTANCE.indexes.get(region).entrySet();

            for (Map.Entry<String, Map<Object, List<Object>>> attrNameToValMapEntry : attrNameToValMapSet) {

                final ValueProcessor valueProcessor = (ValueProcessor)
                        INSTANCE.processors.get(val.getClass()).get(attrNameToValMapEntry.getKey()).clone();

                final Object attrVal = valueProcessor.extractValue(val);

                if (attrNameToValMapEntry.getValue() == null) {
                    attrNameToValMapEntry.setValue(new IndexHashMap<>());
                }

                if (!attrNameToValMapEntry.getValue().containsKey(attrVal)) {
                    attrNameToValMapEntry.getValue().put(attrVal, new LinkedList<>());
                }

                attrNameToValMapEntry.getValue().get(attrVal).add(val);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static void cleanOldValue(Object region, Object oldVal) throws CloneNotSupportedException {

        Set<Map.Entry<String, Map<Object, List<Object>>>> attrNameToValMapSet =
                INSTANCE.indexes.get(region).entrySet();

        for (Map.Entry<String, Map<Object, List<Object>>> attrNameToValMapEntry : attrNameToValMapSet) {

            ValueProcessor valueProcessor = (ValueProcessor)
                    INSTANCE.processors.get(oldVal.getClass()).get(attrNameToValMapEntry.getKey()).clone();

            Object oldAttrVal = valueProcessor.extractValue(oldVal);

            List list = attrNameToValMapEntry.getValue().get(oldAttrVal);
            if (list != null) {
                list.remove(oldVal);
            }
        }
    }

    private static void checkProcessor(Object region, Object val) {
        if (INSTANCE.processors.get(val.getClass()) == null) {
            synchronized (INSTANCE.processors) {
                if (INSTANCE.processors.get(val.getClass()) == null) {
                    generateProcessors(region, val);
                }
            }
        }
    }

    private static void processRemove(Object region, Object key) {
        try {
            Object oldVal = INSTANCE.cache.get(region).get(key);
            if (oldVal != null) {

                if (INSTANCE.indexes.containsKey(region)) {
                    Set<Map.Entry<String, Map<Object, List<Object>>>> attrNameToValMapSet =
                            INSTANCE.indexes.get(region).entrySet();
                    for (Map.Entry<String, Map<Object, List<Object>>> attrNameToValMapEntry : attrNameToValMapSet) {

                        ValueProcessor valueProcessor = (ValueProcessor)
                                INSTANCE.processors.get(oldVal.getClass()).get(attrNameToValMapEntry.getKey()).clone();

                        Object oldAttrVal = valueProcessor.extractValue(oldVal);
                        List list = attrNameToValMapEntry.getValue().get(oldAttrVal);

                        if (list != null) {
                            INSTANCE.indexes.get(region).get(attrNameToValMapEntry.getKey()).
                                    get(oldAttrVal).remove(oldVal);
                        }

                    }
                }
            }
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    private static void generateProcessors(Object region, Object instance) {

        if (instance == null) {
            return;
        }

        Map<String, ValueProcessor> attrProcMap = new HashMap<>();
        for (String o : INSTANCE.indexes.get(region).keySet()) {

            try {
                attrProcMap.put(o, constructClass(instance, o).newInstance());
            } catch (Exception e) {
                throw new RuntimeException("Can't construct class", e);
            }
        }

        INSTANCE.processors.put(instance.getClass(), attrProcMap);
    }

    @SuppressWarnings({"unchecked"})
    private static Class<ValueProcessor> constructClass(Object subject, String fieldName) throws Exception {

        ClassPool classPool = ClassPool.getDefault();
        final Class aClass = subject.getClass();
        final String classname = Cache.class.getName() + "_" + aClass.getName() + "_VallProcessor_" + fieldName;


        CtClass ctClass = classPool.makeClass(classname, classPool.get("ru.xr.cache.ValueProcessor"));

        Method readMethod = null;

        BeanInfo beanInfo = Introspector.getBeanInfo(aClass, Object.class);
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            if (fieldName.equals(propertyDescriptor.getName())) {
                readMethod = propertyDescriptor.getReadMethod();
                break;
            }
        }

        if (readMethod == null) {
            throw new IllegalArgumentException("Read method for field " + fieldName + " not found");
        }

        final String s = constructMethodBody(aClass, readMethod);

        CtMethod ctMethod = CtMethod.make(s, ctClass);

        System.out.println(s);

        ctClass.addMethod(ctMethod);

        return ctClass.toClass();
    }

    private static String constructMethodBody(Class aClass, Method readMethod) throws Exception {

        final String objectMethodCall = "((" + aClass.getName() + ") o)." + readMethod.getName() + "()";

        String methodBody;
        final Class<?> returnType = readMethod.getReturnType();
        if (returnType.isPrimitive()) {
            if (returnType.getCanonicalName().equals("boolean")) {
                methodBody = objectMethodCall + " ? Boolean.TRUE : Boolean.FALSE";
            } else if (returnType.getCanonicalName().equals("char")) {
                methodBody = "new String(" + objectMethodCall + ")";
            } else if (returnType.getCanonicalName().equals("byte")) {
                methodBody = "new Byte(" + objectMethodCall + ")";
            } else if (returnType.getCanonicalName().equals("int")) {
                methodBody = "new Integer(" + objectMethodCall + ")";
            } else if (returnType.getCanonicalName().equals("long")) {
                methodBody = "new Long(" + objectMethodCall + ")";
            } else if (returnType.getCanonicalName().equals("short")) {
                methodBody = "new Short(" + objectMethodCall + ")";
            } else if (returnType.getCanonicalName().equals("double")) {
                methodBody = "new Double(" + objectMethodCall + ")";
            } else if (returnType.getCanonicalName().equals("float")) {
                methodBody = "new Float(" + objectMethodCall + ")";
            } else {
                throw new Exception("Unsupported primitive type: " + returnType.getCanonicalName());
            }
        } else {
            methodBody = objectMethodCall;
        }

        return "public java.lang.Object extractValue(Object o) { return " + methodBody + "; }";
    }

    /**
     * Размещает карту объектов в указанном регионе,
     * в случае присутствия там объекта, он будет заменен на новый
     *
     * @param region регион для размещения
     * @param values карта значений
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    @SuppressWarnings({"unchecked"})
    public static void put(Object region, Map values) throws RegionNotFoundException {

        checkRegion(region);
        Map currentRegion = INSTANCE.cache.get(region);

        try {
            if (!values.isEmpty() && INSTANCE.indexes.containsKey(region)) {
                Set<Map.Entry> entries = values.entrySet();
                for (Map.Entry entry : entries) {
                    processPut(region, entry.getKey(), entry.getValue());
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        currentRegion.putAll(values);
    }

    /**
     * Возвращает объект размещенный в указанном регионе под указанным ключем
     *
     * @param region регион размещения
     * @param key    ключ для получения объекта
     * @return объект размещенный по данным координатам
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    public static Object get(Object region, Object key) throws RegionNotFoundException {
        checkRegion(region);
        return INSTANCE.cache.get(region).get(key);
    }

    /**
     * Возвращает содержимое региона под указанным ключем
     *
     * @param region регион размещения
     * @return объект размещенный по данным координатам
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    public static Map get(Object region) throws RegionNotFoundException {

        checkRegion(region);
        return INSTANCE.cache.get(region);
    }

    /**
     * Удаляет объект размещенный в указанном регионе под указанным ключем
     *
     * @param region регион
     * @param key    ключ объекта
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    public static void remove(Object region, Object key) throws RegionNotFoundException {
        checkRegion(region);

        Object o = INSTANCE.cache.get(region).get(key);

        if (o != null && INSTANCE.processors.containsKey(o.getClass())) {
            processRemove(region, key);
        }

        INSTANCE.cache.get(region).remove(key);
    }

    /**
     * Возвращает набор ключей присутствующих в указанном регионе
     *
     * @param region регион
     * @return ключи
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    public static Set<Object> getKeys(Object region) throws RegionNotFoundException {
        checkRegion(region);
        return INSTANCE.cache.get(region).keySet();
    }

    /**
     * Возвращает содержимое региона
     *
     * @param region идентификатор региона
     * @return содержимое региона
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    public static Collection getRegionContent(Object region) throws RegionNotFoundException {
        Collection result;
        if (INSTANCE.cache.containsKey(region)) {
            result = INSTANCE.cache.get(region).values();
        } else {
            result = Collections.emptyList();
        }

        return result;
    }

    /**
     * Возвращает признак наличия указанного региона
     *
     * @param region регион
     * @return true - регион существует, иначе - false
     */
    public static boolean hasRegion(Object region) {
        return INSTANCE.cache.containsKey(region);
    }

    /**
     * Создает в кеше новый регион
     *
     * @param region создаваемый регион
     */
    public static void createRegion(Object region) {
        INSTANCE.cache.put(region, new ConcurrentHashMap<>());
    }

    private static void checkRegion(Object region) {
        if (!hasRegion(region)) {
            createRegion(region);
        }
    }

    /**
     * Очищает все значения в указанном регионе
     *
     * @param region очищаемый регион
     */
    public static void clearRegion(Object region) {
        INSTANCE.cache.put(region, new ConcurrentHashMap<>());

        if (INSTANCE.indexes.containsKey(region)) {
            if (INSTANCE.indexes.containsKey(region)) {
                Set<Map.Entry<String, Map<Object, List<Object>>>> objectMap = INSTANCE.indexes.get(region).entrySet();
                for (Map.Entry<String, Map<Object, List<Object>>> nameAttrEntries : objectMap) {
                    nameAttrEntries.setValue(new IndexHashMap<>());
                }
            }
        }
    }

    /**
     * Возвращает набор содержащий имена существующих с кеше регионов
     *
     * @return регионы
     */
    public static Set<Object> getRegions() {
        return INSTANCE.cache.keySet();
    }

    /**
     * Удаляет из кеша регион
     *
     * @param region удаляемый регион
     */
    public static void removeRegion(Object region) {
        INSTANCE.cache.remove(region);

        if (INSTANCE.indexes.containsKey(region)) {
            Set<Map.Entry<String, Map<Object, List<Object>>>> objectMap = INSTANCE.indexes.get(region).entrySet();
            for (Map.Entry<String, Map<Object, List<Object>>> nameAttrEntries : objectMap) {
                nameAttrEntries.setValue(new IndexHashMap<>());
            }
        }
    }

    public static Map<String, Map<Object, List<Object>>> getIndexesForRegion(Object regionKey) {
        return INSTANCE.indexes.get(regionKey);
    }

}
