/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IndexHashMap.java 163 2014-07-10 09:31:29Z DGomon $"
 */
public class IndexHashMap<K, V> extends HashMap<K, V> {

    private volatile int inUse = 0;
    private volatile int inClone = 0;


    @Override
    public V put(K key, V value) {

        ++inUse;
        waitClone();
        V v = super.put(key, value);
        --inUse;

        return v;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

        ++inUse;
        waitClone();
        super.putAll(m);
        --inUse;
    }

    public V remove(Object key) {

        ++inUse;
        waitClone();
        V v = super.remove(key);
        --inUse;

        return v;
    }

    @Override
    public void clear() {

        ++inUse;
        waitClone();
        super.clear();
        --inUse;
    }

    private void waitClone() {
        while (inClone > 0) {
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SuppressWarnings({"unchecked"})
    public K[] getKeys() {

        ++inClone;
        while (inUse > 0) {
            --inClone;
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                ++inClone;
            }
        }

        K[] ks = (K[]) this.keySet().toArray();

        --inClone;

        return ks;
    }
}
