/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.cache;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ValueProcessor.java 163 2014-07-10 09:31:29Z DGomon $"
 */
public abstract class ValueProcessor implements Cloneable {

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public abstract Object extractValue(Object o);

}
