/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import ru.xr.rims.common.Configuration;
import ru.xr.rims.common.ServerArray;
import ru.xr.rims.services.LockProperties;
import ru.xr.rims.services.TimeoutException;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Обработчик блокировок
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Lock.java 90 2011-06-07 12:23:14Z dgomon $"
 */
@SuppressWarnings("WeakerAccess")
public class Lock {

    private static Logger logger = Logger.getLogger(Lock.class.getName());

    /** Заглушка для использования локов вне транзакций */
    public static final Object NULL_CONNECTION = new Object();

    /** Максимальное время ожидания постановки блокировки */
    public static final long LOC_REQUEST_TIMEOUT = Configuration.getInstance().getLocRequestTimeout();

    /** Хранилище блокировок */
    private static final Map<Object, Set<LockProperties>> LOCKS = new HashMap<>();

    /** Приемник запросов на блокировку */
    private static final BlockingQueue<Thread> BLOCKING_QUEUE = new LinkedBlockingDeque<>();

    /** Хранилище запросов на блокировку, ожидающих необходимый ресурс */
    private static final Map<Object, LinkedList<Object>> FRIZZED = new HashMap<>();

    /** Признак обратимой приостановки обработки очереди запросов на блокировку */
    private static boolean run = false;

    /** Признак окончательной остановки обработки очереди запросов на блокировку */
    private static boolean alwaysRun = true;

    static {
        createAndStartLockConsumer();
    }

    /** Метод создает и запускает поток для обработки запросов на блокировку */
    private static void createAndStartLockConsumer() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    while (alwaysRun) {

                        BLOCKING_QUEUE.take().start();
                        logger.log(Level.FINE, "QUEUE take, size: {0}", BLOCKING_QUEUE.size());
                        while (!run) {
                            //todo: в настройки!!!
                            Thread.sleep(10);
                        }
                    }
                } catch (InterruptedException e) {
                    if (logger.isLoggable(Level.FINE)) {
                        logger.log(Level.FINE, e.getMessage(), e);
                    } else {
                        logger.info(e.getMessage());
                    }
                }
            }

        }, "LOCKER_EXECUTOR").start();
    }

    /** Приостановить обработку поступающих запросов на блокировку */
    public static void stop() {
        run = false;
    }

    /** Возобновить обработку поступающих запросов на блокировку */
    public static void start() {
        run = true;
    }

    /**
     * Выставляет блокировку на объект в рамках сессии
     *
     * @param connectionId идентификатор соединения, сессии
     * @param region       регион объекта
     * @param key          ключ объекта
     * @param timeOut      таймаут блокировки
     * @param local        признак распостроекния блокировки на соседние сервера
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    public static void lock(Object connectionId, final Object region, final Object key,
            final long timeOut, boolean local) throws TimeoutException {

        lock(connectionId, connectionId, region, key, timeOut, local);
    }

    /**
     * Выставляет блокировку на объект в рамках сессии
     *
     * @param details      доп. инфо для опозанания запроса на блокировку и самой блокировки
     * @param connectionId идентификатор соединения, сессии
     * @param region       регион объекта
     * @param key          ключ объекта
     * @param timeOut      таймаут блокировки
     * @param local        признак распостроекния блокировки на соседние сервера
     * @throws ru.xr.rims.services.TimeoutException при превышении времени ожидания постановки блокировки
     */
    public static void lock(Object details, Object connectionId, final Object region, final Object key,
            final long timeOut, boolean local) throws TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Lock received [{0}]", connectionId.toString());
        }

        final Locker target = new Locker(connectionId, region, key, timeOut, local, details);
        final Thread locker = new Thread(target, "Locker_" + region + "_" + key);

        BLOCKING_QUEUE.add(locker);

        while (!target.isComplete() && target.isActual()) {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Lock applying wait [{0}]", connectionId.toString());
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                if (logger.isLoggable(Level.FINE)) {
                    logger.log(Level.FINE, e.getMessage(), e);
                } else {
                    logger.warning(e.getMessage());
                }
            }
        }

        if (!target.isActual()) {
            throw new TimeoutException("Lock timeout: [" + connectionId + "] Region [" + target.region
                    + "]; Key [" + target.key + "], Complete: " + target.isComplete());
        }
    }

    /**
     * Снимает блокировку с объекта
     *
     * @param connectionId идентификатор соединения (сессии)
     * @param region       регион кеша
     * @param key          ключ объекта
     * @param timeOut      максимально время ожидания
     * @param local        признак локальной блокировки
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public static void unLock(Object connectionId, Object region, Object key, long timeOut, boolean local) {

        if (LOCKS.containsKey(region)) {

            final LockProperties lockProperties = new Locker.LockWrapper(key, connectionId);
            if (LOCKS.get(region).contains(lockProperties)) {
                if (!local) {
                    for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
                        try {
                            server.getServerCache().unlock(region, key);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }

            synchronized (LOCKS) {
                LOCKS.get(region).remove(lockProperties);
            }
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Lock removed: connectionId[{0}], region[{1}], key[{2}]",
                        new Object[]{connectionId.toString(), region, key});
            }
        }
    }

    /**
     * Возвращает карту блокировок на объектах в кеше
     *
     * @return блокировки
     */
    static synchronized Map<Object, Set<LockProperties>> getLocks() {
        synchronized (LOCKS) {
            return LOCKS;
        }
    }

    /**
     * Удаляет существующую блокировку
     *
     * @param connectionId идентификатор сессии
     * @param region       идентификатор региона
     * @param key          ключ блокировки
     */
    static void removeLock(Object connectionId, Object region, Object key) {

        final LockProperties lockProperties = new Locker.LockWrapper(key, connectionId);
        synchronized (LOCKS) {
            LOCKS.get(region).remove(lockProperties);
            if (region.equals(key)) {
                FRIZZED.remove(region);
            }
        }
    }

    /** Класс осуществляющий постановку блокировки */
    static class Locker implements Runnable {

        /** Идентификатор региона */
        private final Object region;
        /** Ключ блокировки */
        private final Object key;

        private final Object details;

        /** Признак локальной блокировки, при значении  true - блокировка не распостраняется на соседние сервера */
        private final boolean local;
        /** Время ожидания постановки блокировки, по истечении которого будет выброшено исключение */
        private final long timeOut;

        /** Идентификатор сессии */
        private final Object connectionId;
        /** Признак актуальности блокировки */
        private volatile boolean actual = true;
        /** Признак получения доступа к блокировке */
        private volatile boolean accessed = false;
        /** Признак выполнения блокировки */
        private volatile boolean complete = false;

        final LockWrapper lockWrapper;

        /**
         * Конструирует новый инстанс блокировщика
         *
         * @param connectionId идентификатор сессии
         * @param region       идентификатор региона
         * @param key          ключ блокировки
         * @param timeOut      время ожидания постановки блокировки, по истечении которого будет выброшено исключение
         * @param local        признак локальной блокировки,
         *                     при значении  true - блокировка не распостраняется на соседние сервера
         * @param details      доп. инфо для опозанания запроса на блокировку и самой блокировки
         */
        private Locker(Object connectionId, Object region, Object key, long timeOut, boolean local, Object details) {
            this.connectionId = connectionId;
            this.region = region;
            this.key = key;
            this.timeOut = timeOut;
            this.local = local;
            this.details = details;

            this.lockWrapper = new LockWrapper(this.region, this.key, this.connectionId, details);

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE,
                        "Lock request created: connectionId[{0}], region[{1}]; key[{2}]; local[{3}]; timeout[{4}]",
                        new Object[]{connectionId.toString(), this.region, this.key, this.local, this.timeOut});
            }

        }

        /**
         * Возвращает признак выполнения блокировки
         *
         * @return true - блокировка выполнена, иначе - false
         */
        public boolean isComplete() {
            return this.complete;
        }

        /**
         * Возвращает признак актуальности блокировки
         *
         * @return При превышении времени ожидания постановки
         *         на блокировку вернет <code>false</code>, иначе - <code>true</code>
         */
        public boolean isActual() {
            return actual;
        }

        @Override
        public void run() {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Run Locker: sessionId[{0}] Region: [{1}] Key: [{2}]",
                        new Object[]{this.connectionId.toString(), region, key});
            }

            final Timer timer = startTimer();

            final Set<LockProperties> lockProps;
            final LinkedList<Object> frizzedList;

            synchronized (LOCKS) {

                verifyRegionExistence();

                lockProps = LOCKS.get(region);
                frizzedList = FRIZZED.get(region);

                // запрвшиваем доступность блокировки
                askForLock(lockProps, frizzedList);
                this.accessed = true;

                // отменяем ранее запущенный таймер
                timer.cancel();

                if (isActual()) {
                    lockProps.add(lockWrapper);
                    // убираем из заморозки
                    frizzedList.remove(this);
                }
            } //synchronized (LOCKS)

            if (isActual()) {
                // распостраняем на соседние сервера...
                if (!local) {
                    try {
                        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
                            server.getServerCache().lock(region, key);
                        }
                    } catch (Exception e) {
                        logger.log(Level.WARNING, e.getMessage(), e);
                        synchronized (LOCKS) {
                            lockProps.remove(lockWrapper);
                        }
                        throw new RuntimeException(e);
                    }
                }

                complete = true;

                if (logger.isLoggable(Level.FINE)) {
                    logger.log(Level.FINE, "Lock applied: sessionId[{0}] Region: [{1}] Key: [{2}]",
                            new Object[]{lockWrapper.getSessionId().toString(), region, key});
                }
            }

        }

        private void verifyRegionExistence() {

            if (!LOCKS.containsKey(region)) {
                LOCKS.put(region, new HashSet<>());
            }

            if (!FRIZZED.containsKey(region)) {
                FRIZZED.put(region, new LinkedList<>());
            }
        }

        /**
         * Проверяет соблюдение условий постановки на блокировку, если доступно, возвращаем поток управления,
         * если нет, то ждем пока не получим разрешение или пока не упадем...
         *
         * @param lockProps   проверяемый набор блокировок
         * @param frizzedList список блокировок в ожидании
         */
        private void askForLock(Set<LockProperties> lockProps, LinkedList<Object> frizzedList) {

            // инкремент для ожидания постановки на блокировку
            int incWait = 1;

            if (!lockProps.isEmpty()) {

                // проверяем возможность размещения блокировки
                while (waitOnKey(lockProps, frizzedList)) {

                    if (lockWrapper.isLockOnRegion()) {

                        // если в наборе ожидающих нет этого лока, добавляем в очередь ожидания
                        if (!frizzedList.contains(this)) {
                            frizzedList.add(this);
                            // если есть, то проверяем верхний элемент, и если это он,
                            //  и больше локов нет, то толкаем его дальше и выходим из while
                        } else if (this.equals(frizzedList.get(0)) && lockProps.isEmpty()) {
                            FRIZZED.remove(0);
                            break;
                        }
                    }

                    // тупо, временно отпускаем блокировку на объекте LOCKS
                    try {
                        LOCKS.wait(incWait < 30 ? incWait++ : incWait);

                        if (!isActual()) {
                            if (frizzedList.contains(this)) {
                                frizzedList.remove(this);
                            }
                            break;
                        }
                    } catch (InterruptedException e) {
                        if (logger.isLoggable(Level.WARNING)) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                        }

                        break;
                    }
                }  // while
            }      //if (!LOCKS.containsKey(region))
        }

        /**
         * Проверяет наличие аналогичной блокировки в другой сессии или в ожидающих
         *
         * @param lockProps   проверяемый набор блокировок
         * @param frizzedList список блокировок в ожидании
         * @return признак недоступности блокировки
         */
        private boolean waitOnKey(Set<LockProperties> lockProps, LinkedList<Object> frizzedList) {

            boolean result = false;

            // проверим существование, в локах необходимого региона, требуемого ключа
            final boolean hasLockOnKey = lockProps.contains(lockWrapper);
            // когда хочу весь регион
            final boolean wannaRegion = lockWrapper.isLockOnRegion();

            // если ключик в локах или его нет но хотим залочить весь регион...
            if (hasLockOnKey || wannaRegion) {

                if (wannaRegion) {
                    // весь регион
                    result = hasOtherSessionId(lockProps);

                } else {
                    // ключик в локах
                    result = !hasLockOnKeyInSession(lockProps);
                }
            }

            // если хачу регион, такого ключика нет в локах, и прочих граблей нет, то осталось
            // проверить что нет ожидающих, пришедших раньше запросов на блокировку региона...
            if (wannaRegion && !hasLockOnKey && !result) {
                result = frizzedList.isEmpty();
            }

            return result;
        }

        /**
         * Проверяет наличие блокировки от другой сессии
         *
         * @param lockWrappers проверяемый набор блокировок
         * @return true - блокировки от других сессий есть, иначе - false
         */
        private boolean hasOtherSessionId(Set<LockProperties> lockWrappers) {

            boolean result = false;

            for (LockProperties wrapper : lockWrappers) {
                if (!wrapper.getSessionId().equals(connectionId)) {
                    // если находим не для этой сессии, то говорим что надо ждать
                    result = true;
                    break;
                }
            }

            return result;
        }

        /**
         * Проверяет наличие блокировки на ключе в текущей сессии
         *
         * @param lockWrappers проверяемый набор блокировок
         * @return <b>true</b> - блокировка наша, иначе - <b>false</b>
         */
        private boolean hasLockOnKeyInSession(Set<LockProperties> lockWrappers) {

            boolean result = false;

            for (LockProperties wrapper : lockWrappers) {
                if (wrapper.equals(lockWrapper) && wrapper.getSessionId().equals(connectionId)) {
                    result = true;
                    break;
                }
            }

            return result;
        }

        /**
         * Конструирует и запускает таймер выставляющий на локере признак актуальности в false через количество мс
         * указаных в {@link #timeOut}
         *
         * @return запущенный таймер
         */
        private Timer startTimer() {

            // таймер для отлупа запросов на блокировку
            final Timer timer = new Timer("Locker timer [" + connectionId.toString() + "]," + region + ":" + key, true);
            timer.schedule(new TimerTask() {

                @Override
                public void run() {

                    if (logger.isLoggable(Level.FINE)) {
                        logger.log(Level.FINE, "Lock rejected [{0}], Region: [{1}], Key: [{2}]",
                                new Object[]{connectionId.toString(), Locker.this.region, Locker.this.key});
                    }

                    if (!Locker.this.accessed) {
                        Locker.this.actual = false;
                    }
                }
            }, Locker.this.timeOut);

            return timer;
        }

        /**
         * Возвращает допинфо по блокировке
         *
         * @return детали
         */
        @SuppressWarnings({"UnusedDeclaration"})
        public Object getDetails() {
            return details;
        }

        /** Контейнер для хранения необходимой информации в контексте блокировки */
        static class LockWrapper implements LockProperties {

            /** Идентификатор региона для блокировки */
            private final Object region;
            /** Ключ для блокировки */
            private final Object key;
            /** Идентификатор сессии */
            private final Object sessionId;
            /** Вспомогательная информация для идентификации блокировки */
            private final Object details;

            private final boolean lockOnRegion;

            /**
             * Конструирует новый контейнер с указанными ключем и идентификатором сессии
             *
             * @param region    региона для блокировки
             * @param key       ключ блокировки
             * @param sessionId идентификатор сессии
             * @param details   вспомогательная информация для идентификации блокировки
             */
            private LockWrapper(Object region, Object key, Object sessionId, Object details) {

                this.region = region;
                this.key = key;
                this.sessionId = sessionId;
                this.details = details;

                this.lockOnRegion = key.equals(region);
            }

            /**
             * Конструирует новый контейнер с указанными ключем и идентификатором сессии
             *
             * @param key       ключ блокировки
             * @param sessionId идентификатор сессии
             */
            private LockWrapper(Object key, Object sessionId) {
                this(null, key, sessionId, null);
            }

            /**
             * Возвращает ключ блокировки
             *
             * @return ключ
             */
            public Object getKey() {
                return key;
            }

            /**
             * Возвращает идентификатор сессии
             *
             * @return идентификатор сессии
             */
            public Object getSessionId() {
                return sessionId;
            }

            /**
             * Возвращает вспомогательную информацию  для идентификации блокировки
             *
             * @return инфа
             */
            @SuppressWarnings({"UnusedDeclaration"})
            public Object getDetails() {
                return details;
            }

            /**
             * Возвращает идентификатор региона
             *
             * @return регион
             */
            @SuppressWarnings({"UnusedDeclaration"})
            public Object getRegion() {
                return region;
            }

            /**
             * Возвращает признак блокировки на регионе
             *
             * @return true - на регионе, иначе - false
             */
            public boolean isLockOnRegion() {
                return lockOnRegion;
            }

            /**
             * Переопределено с целью учета исключительно ключа блокировки
             *
             * @return hashCode от ключа блокировки
             */
            @Override
            public int hashCode() {
                return key.hashCode();
            }

            /**
             * Переопределено с целью учета исключительно ключа блокировки
             *
             * @return true - если ключи блокировки совпадают, иначе - false
             */
            @Override
            public boolean equals(Object o) {

                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }

                final LockWrapper that = (LockWrapper) o;

                return !(key != null ? !key.equals(that.key) : that.key != null);
            }
        }
    }

}
