/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMISocketFactory;

/**
 * Реализация RMISocketFactory, позволяющая разносить по разным сетевым интерфейсам сервисы, поднятые на одной Registry
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: StorageRMISocketFactory.java 52 2010-11-01 16:54:36Z dgomon $"
 */
class StorageRMISocketFactory extends RMISocketFactory {

    /** хост на котором создаются сокеты */
    private String host;

    /**
     * Конструирует новую фабрику создающую сокеты на указанном хосте
     *
     * @param host хост для привязки
     */
    StorageRMISocketFactory(String host) {
        this.host = host;
    }

    @Override
    public ServerSocket createServerSocket(int port) throws IOException {

        InetSocketAddress isa = new InetSocketAddress(host, port);
        ServerSocket ss = new ServerSocket();
        ss.bind(isa);

        return ss;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        return getDefaultSocketFactory().createSocket(host, port);
    }
}
