/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server.admin;

import ru.xr.rims.common.Application;
import ru.xr.rims.common.ExitForCode;
import ru.xr.rims.common.ServerArray;
import ru.xr.rims.server.admin.commands.Command;
import ru.xr.rims.server.admin.commands.Exit;
import ru.xr.rims.server.admin.commands.Ping;
import ru.xr.rims.server.admin.commands.Show;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Admin.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public class Admin extends Application {

    public static void main(String[] args) {
        Admin admin = new Admin();
        admin.parseArgs(args);
        try {
            admin.init();
            ServerArray.getArray().getCurrent().initRegistry();
            admin.prepareCommands();
        } catch (RemoteException e) {
            e.printStackTrace();
            admin.exit(ExitForCode.errorInitialization, e.getMessage());
        }

        while (true) {
            System.out.print("##>");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                String inCommandLine = in.readLine();

                if (inCommandLine != null) {
                    inCommandLine = inCommandLine.trim();
                    if (!inCommandLine.isEmpty()) {
                        String[] strings = inCommandLine.trim().split("\\s");

                        if (strings.length > 0) {
                            Command command = Command.getCommand(strings[0].trim());
                            if (command != null) {
                                command.exec(inCommandLine);
                            } else {
                                System.out.println("Incorrect command");
                                System.out.println(" : " + Command.getNames());
                            }
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void prepareCommands() {
        new Exit(this);
        new Show();
        new Ping();
    }
}

//        try {
//            ServerStorage storage = ServerArray.getArray().getCurrent().getServerCache();
//            storage.stopRequest();
//            while (ServerStorage.PING.equals(ServerArray.getArray().getCurrent().getServerCache().ping())) {
//                System.out.println("Stop completion waiting...");
//            }
//        } catch (ConnectException e) {
//            System.out.println("Server not available");
//        } catch (UnmarshalException e) {
//            System.out.println("Probably server stopped");
//        } catch (Throwable throwable) {
//            System.out.println("Probably server stopped");
//        }
