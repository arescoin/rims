/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server.admin.commands;

import ru.xr.rims.common.Application;
import ru.xr.rims.common.ExitForCode;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Exit.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public class Exit extends Command {

    @SuppressWarnings("WeakerAccess")
    public static final String NAME = "exit";

    private final Application application;

    public Exit(Application application) {
        super(NAME);
        this.application = application;
    }

    @Override
    public void exec(String commandLine) {
        application.exit(ExitForCode.ok, "Admin tool");
    }
}
