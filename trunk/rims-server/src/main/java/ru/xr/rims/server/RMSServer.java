/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import ru.xr.cache.Cache;
import ru.xr.rims.common.Application;
import ru.xr.rims.common.Configuration;
import ru.xr.rims.common.ExitForCode;
import ru.xr.rims.common.ServerArray;
import ru.xr.rims.services.RemoteStorage;
import ru.xr.rims.services.ServerStorage;

import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Приложение сервера удаленного кеша
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSServer.java 60 2010-11-15 12:14:01Z dgomon $"
 */
@SuppressWarnings("WeakerAccess")
public class RMSServer extends Application {

    private static Logger logger = Logger.getLogger(RMSServer.class.getName());

    /** Серверный индекс в массивах */
    public static final int SERVER_INDEX = 0;
    /** Клиентский индекс в массивах */
    public static final int CLIENT_INDEX = 1;

    /** Инстансы сервисов */
    public static final Remote[] REMOTES_SRC = new Remote[]{new ServerStorageImpl(), new RemoteStorageImpl()};
    /** Имена сервисов */
    public static final String[] NAMES = new String[]{ServerStorage.REMOTE_ELEMENT, RemoteStorage.REMOTE_ELEMENT};
    /** Стабы сервисов */
    public static final Remote[] REMOTES = new Remote[2];
    /** Реестры */
    public static final Registry[] REGISTRIES = new Registry[2];

    /** Инстанс сервера */
    static RMSServer server;

    /** Признак актуальности задачи обновления объектов в реестре */
    private boolean doRebind = true;
    /** Период обновления объектов в реестре */
    private long rebindPeriod;


    /**
     * Создает, настраивает и запускает приложение
     *
     * @param args аргументы командной строки для запуска
     * @see Argument
     */
    public static void main(String[] args) {
        System.setSecurityManager(new RMISecurityManager());

        server = new RMSServer();
        server.parseArgs(args);

        try {
            server.init();
        } catch (RemoteException e) {
            if (logger.isLoggable(Level.FINEST)) {
                logger.log(Level.FINEST, e.getMessage(), e);
            } else {
                logger.info(e.getMessage());
            }
            server.exit(ExitForCode.errorInitialization, e.getMessage());
        }

        server.start();

        logger.info("Server started.");
    }

    protected void init(String configFileName) throws RemoteException {
        super.init(configFileName);
        Lock.start();
        this.rebindPeriod = Configuration.getInstance().getRebindPeriod();
        ServerArray.getArray().startMonitor();
        initServerRegistry();
        synchronizeData();
    }

    /** инициализирует реестр серверной части */
    private void initServerRegistry() {
        initRegistry(SERVER_INDEX);
        logger.info("Remote Server registered");

    }

    /** инициализирует реестр клиентской части */
    private void initClientRegistry() {
        initRegistry(CLIENT_INDEX);
        logger.info("Remote Storage registered");
    }

    /**
     * инициализирует реестр с указанным индексом
     *
     * @param registryIndex индекс,  {@link #CLIENT_INDEX клиентский}, лобо {@link #SERVER_INDEX серверный}
     */
    private void initRegistry(int registryIndex) {
        try {
            ServerArray.Server server = ServerArray.getArray().getCurrent();

            String host = obtainHostName(registryIndex, server);
            int port = obtainPort(registryIndex, server);

            StorageRMISocketFactory socketFactory = new StorageRMISocketFactory(host);
            REGISTRIES[registryIndex] = LocateRegistry.createRegistry(port, socketFactory, socketFactory);
            REMOTES[registryIndex] = UnicastRemoteObject.exportObject(REMOTES_SRC[registryIndex], 0);

            startRebind(registryIndex);
        } catch (RemoteException e) {
            if (logger.isLoggable(Level.FINEST)) {
                logger.log(Level.FINEST, e.getMessage(), e);
            } else {
                logger.info(e.getMessage());
            }
            ExitForCode.errorConfig.exit();
        }
    }

    /**
     * получает значение порта для указанного сервера на указанном типе интерфейса
     *
     * @param typeIndex индекс,  {@link #CLIENT_INDEX клиентский}, лобо {@link #SERVER_INDEX серверный}
     * @param server    инстанс сервера
     * @return номер порта
     */
    private int obtainPort(int typeIndex, ServerArray.Server server) {
        int port;

        if (typeIndex == SERVER_INDEX) {
            port = server.getPortServer();
        } else {
            port = server.getPortClient();
        }

        return port;
    }

    /**
     * получает значение имя хоста для указанного сервера на указанном типе интерфейса
     *
     * @param typeIndex индекс,  {@link #CLIENT_INDEX клиентский}, лобо {@link #SERVER_INDEX серверный}
     * @param server    инстанс сервера
     * @return хост
     */
    private String obtainHostName(int typeIndex, ServerArray.Server server) {
        String host;
        if (typeIndex == SERVER_INDEX) {
            host = server.getHostServer();
        } else {
            host = server.getHostClient();
        }

        return host;
    }

    /**
     * Создает и запускает поток обновляющий объекты в реестре
     *
     * @param index индекс реестра ,  {@link #CLIENT_INDEX клиентский}, лобо {@link #SERVER_INDEX серверный}
     */
    private void startRebind(final int index) {

        final boolean[] started = {false};

        Thread thread = new Thread("Thread Rebinder: " + NAMES[index]) {

            @Override
            public void run() {
                super.run();

                started[0] = true;
                while (doRebind) {
                    try {
                        REGISTRIES[index].rebind(NAMES[index], REMOTES[index]);
                        logger.info("Registry updated: " + NAMES[index]);
                        Thread.sleep(RMSServer.this.rebindPeriod);
                    } catch (RemoteException | InterruptedException e) {
                        if (logger.isLoggable(Level.FINEST)) {
                            logger.log(Level.FINEST, e.getMessage(), e);
                        } else {
                            logger.info(e.getMessage());
                        }
                    }
                }
            }
        };

        thread.setDaemon(true);
        thread.start();

        while (!started[0]) {
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                if (logger.isLoggable(Level.FINEST)) {
                    logger.log(Level.FINEST, e.getMessage(), e);
                } else {
                    logger.info(e.getMessage());
                }
            }
        }
    }

    /** Затягивает данные из соседнего сервера */
    private void synchronizeData() {

        logger.info("Try to synchronize data...");
        Set<ServerArray.Server> servers = ServerArray.getArray().getServersOnLine();
        logger.info("ServerArray.getArray().getServersOnLine().size() = [" + servers.size() + "]");

        if (!servers.isEmpty()) {
            ServerArray.Server server = servers.iterator().next();

            logger.info("Selecting donor server: [" + server + "]");

            try {
                RemoteStorage remoteStorage = server.getRemoteCache();
                Set regions = remoteStorage.getRegions();

                logger.info("Regions size: [" + regions.size() + "]");
                for (Object region : regions) {
                    logger.info("Start region reading [" + region + "]");
                    Set keys = remoteStorage.getKeys(region);
                    logger.info("Region size [" + keys.size() + "]");
                    Cache.createRegion(region);
                    for (Object key : keys) {
                        Cache.put(region, key, remoteStorage.get(region, key));
                    }
                    logger.info("End of region reading [" + region + "]");
                }

            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                exit(ExitForCode.errorStartUpSynchronization);
            }
        }

        logger.info("Data synchronization complete.");
    }

    /** Запускает обслуживание на клиентской стороне */
    private void start() {
        initClientRegistry();
        logger.info("Client side started.");
    }

    /** Останавливает инстанс сервера */
    public void stop() {
        ServerArray.getArray().stopMonitor();
        Lock.stop();
        doRebind = false;

        for (Registry registry : REGISTRIES) {
            clearRegistry(registry);
        }

        super.stop();
    }

    /**
     * Очищает реестр
     *
     * @param registry реестр для очистки
     */
    private void clearRegistry(Registry registry) {
        if (registry != null) {
            try {
                for (String name : registry.list()) {
                    registry.unbind(name);
                }
            } catch (RemoteException | NotBoundException e) {
                if (logger.isLoggable(Level.FINEST)) {
                    logger.log(Level.FINEST, e.getMessage(), e);
                } else {
                    logger.info(e.getMessage());
                }
            }
        }
    }

}
