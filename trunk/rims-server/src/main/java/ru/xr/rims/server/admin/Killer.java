/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server.admin;

import ru.xr.rims.common.Application;
import ru.xr.rims.common.ExitForCode;
import ru.xr.rims.common.ServerArray;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnmarshalException;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Killer.java 60 2010-11-15 12:14:01Z dgomon $"
 */
public class Killer extends Application {

    public static void main(String[] args) {
        Killer admin = new Killer();
        admin.parseArgs(args);

        try {
            admin.init();
            ServerArray.getArray().getCurrent().initRegistry();
            ServerArray.getArray().getCurrent().getServerCache().stopRequest();
        } catch (UnmarshalException e) {
            System.out.println("Maybe ok");
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
            admin.exit(ExitForCode.errorInitialization, e.getMessage());
        }


    }

}
