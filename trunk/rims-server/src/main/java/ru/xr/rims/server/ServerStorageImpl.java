/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import ru.xr.cache.Cache;
import ru.xr.cache.RegionNotFoundException;
import ru.xr.rims.common.ExitForCode;
import ru.xr.rims.services.LockProperties;
import ru.xr.rims.services.ServerStorage;
import ru.xr.rims.services.TimeoutException;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Реализация набора интерфейсов предоставляющих сервисы клиентской части
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ServerStorageImpl.java 72 2011-04-18 13:14:48Z dgomon $"
 */
public class ServerStorageImpl implements ServerStorage {

    @Override
    public void lock(Object region, Object key) throws RemoteException, TimeoutException {
        Lock.lock(Lock.NULL_CONNECTION, region, key, Lock.LOC_REQUEST_TIMEOUT, true);
    }

    @Override
    public void unlock(Object region, Object key) throws RemoteException {
        Lock.unLock(Lock.NULL_CONNECTION, region, key, Lock.LOC_REQUEST_TIMEOUT, true);
    }

    @Override
    public void put(Object region, Object key, Object val) throws RemoteException, RegionNotFoundException {
        Cache.put(region, key, val);
    }

    @Override
    public void put(Object region, Map values) throws RemoteException, RegionNotFoundException {
        Cache.put(region, values);
    }

    @Override
    public Object get(Object region, Object key) throws RemoteException, RegionNotFoundException {
        return Cache.get(region, key);
    }

    @Override
    public Map getRegionContent(Object region) throws RemoteException, RegionNotFoundException {
        return Cache.get(region);
    }

    @Override
    public void remove(Object region, Object key) throws RemoteException, RegionNotFoundException {
        Cache.remove(region, key);
    }

    @Override
    public Set<Object> getKeys(Object region) throws RemoteException, RegionNotFoundException {
        return new HashSet<>(Cache.getKeys(region));
    }

    @Override
    public boolean hasRegion(Object region) throws RemoteException {
        return Cache.hasRegion(region);
    }

    @Override
    public void createRegion(Object region) throws RemoteException {
        Cache.createRegion(region);
    }

    @Override
    public void clearRegion(Object region) throws RemoteException {
        Cache.clearRegion(region);
    }

    @Override
    public Set<Object> getRegions() throws RemoteException {
        return new HashSet<>(Cache.getRegions());
    }

    @Override
    public void removeRegion(Object region) throws RemoteException {
        Cache.removeRegion(region);
    }

    @Override
    public String ping() {
        return ServerStorage.PING;
    }

    @Override
    public void runGC() throws RemoteException {
        System.gc();
    }

    @Override
    public Map<Object, Set<LockProperties>> getLocks() {
        return Lock.getLocks();
    }

    @Override
    public Set<LockProperties> getLocksForRegion(Object region) {
        return Lock.getLocks().get(region);
    }

    @Override
    public void killLock(Object connectionId, Object region, Object key) {
        Lock.removeLock(connectionId, region, key);
    }

    @Override
    public void stopRequest() throws RemoteException {
        if (RMSServer.server != null) {
            RMSServer.server.exit(ExitForCode.ok, "Admin request");
        } else {
            throw new IllegalStateException("Server not available");
        }
    }
}
