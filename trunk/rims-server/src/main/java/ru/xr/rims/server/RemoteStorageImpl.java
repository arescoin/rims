/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import ru.xr.cache.Cache;
import ru.xr.cache.RegionNotFoundException;
import ru.xr.rims.common.ServerArray;
import ru.xr.rims.services.GlobalService;
import ru.xr.rims.services.StorageFilter;
import ru.xr.rims.services.TimeoutException;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Реализация набора интерфейсов предоставляющих сервисы клиентской части
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RemoteStorageImpl.java 77 2011-05-06 08:02:19Z dgomon $"
 */
public class RemoteStorageImpl implements GlobalService {

    private static Logger logger = Logger.getLogger(RemoteStorageImpl.class.getName());

    @Override
    public void put(Object region, Object key, Object val) throws RemoteException, RegionNotFoundException {

        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
            try {
                server.getServerCache().put(region, key, val);
            } catch (NotBoundException e) {
                throw new RemoteException(e.getMessage(), e);
            }
        }

        Cache.put(region, key, val);
    }

    @Override
    public void put(Object region, Map values) throws RemoteException, RegionNotFoundException, TimeoutException {

        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
            try {
                server.getServerCache().put(region, values);
            } catch (NotBoundException e) {
                throw new RemoteException(e.getMessage(), e);
            }
        }

        Cache.put(region, values);
    }

    @Override
    public void putMap(Object region, byte[] values) throws RemoteException, RegionNotFoundException, TimeoutException {

        Map map = (Map) ByteConverter.bytesToObject(values);

        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
            try {
                server.getServerCache().put(region, map);
            } catch (NotBoundException e) {
                throw new RemoteException(e.getMessage(), e);
            }
        }

        Cache.put(region, map);
    }

    @Override
    public Map getRegionContent(Object region) throws RemoteException, RegionNotFoundException {
        return Cache.get(region);
    }

    @Override
    public byte[] getRegionContentAsBytes(Object region) throws RemoteException, RegionNotFoundException {

        Map map = Cache.get(region);

        return ByteConverter.objectToBytes(map);
    }

    @Override
    public Object get(Object region, Object key) throws RemoteException, RegionNotFoundException {
        return Cache.get(region, key);
    }

    @Override
    public void remove(Object region, Object key) throws RemoteException, RegionNotFoundException {

        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
            try {
                server.getServerCache().remove(region, key);
            } catch (NotBoundException e) {
                throw new RemoteException(e.getMessage(), e);
            }
        }

        Cache.remove(region, key);
    }

    @Override
    public Set<Object> getKeys(Object region) throws RemoteException, RegionNotFoundException {
        return new HashSet<>(Cache.getKeys(region));
    }

    @Override
    public boolean hasRegion(Object region) throws RemoteException {
        return Cache.hasRegion(region);
    }

    @Override
    public void clearRegion(Object region) throws RemoteException {

        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
            try {
                server.getServerCache().clearRegion(region);
            } catch (NotBoundException e) {
                throw new RemoteException(e.getMessage(), e);
            }
        }

        Cache.clearRegion(region);
    }

    @Override
    public void createRegion(Object region) throws RemoteException {
        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {

            try {
                server.getServerCache().createRegion(region);
            } catch (NotBoundException e) {
                throw new RemoteException(e.getMessage(), e);
            }
        }

        Cache.createRegion(region);
    }

    @Override
    public void removeRegion(Object region) throws RemoteException {

        for (ServerArray.Server server : ServerArray.getArray().getServersOnLine()) {
            try {
                server.getServerCache().removeRegion(region);
            } catch (NotBoundException e) {
                throw new RemoteException(e.getMessage(), e);
            }
        }

        Cache.removeRegion(region);
    }

    @Override
    public Set<Object> getRegions() throws RemoteException {
        return new HashSet<>(Cache.getRegions());
    }

    @Override
    public void lock(Object region, Object key) throws RemoteException, TimeoutException {
        Lock.lock(Lock.NULL_CONNECTION, region, key, Lock.LOC_REQUEST_TIMEOUT, false);
    }

    @Override
    public void unlock(Object region, Object key) throws RemoteException {
        Lock.unLock(Lock.NULL_CONNECTION, region, key, Lock.LOC_REQUEST_TIMEOUT, false);
    }

    @Override
    public Collection searchObjects(Object region, StorageFilter filter) throws RemoteException {

        Collection filterResult = filter.filter(Cache.getRegionContent(region));

        if (filter.isExtractionRequiered()) {
            filterResult = filter.extractValues(filterResult);
        }

        return filterResult;
    }

    public Collection searchObjects(Collection<StorageFilter> filters) throws RemoteException {

        Collection extractedValues = null;
        Collection filterResult = null;

        for (StorageFilter filter : filters) {

            if (extractedValues != null) {
                filter.prepareCriterionValues(extractedValues);
            }

            filterResult = filter.filter(Cache.getRegionContent(filter.getRegionKey()));
            extractedValues = filter.extractValues(filterResult);
        }

        if (extractedValues != null && !extractedValues.isEmpty()) {
            filterResult = extractedValues;
        }

        return filterResult;
    }

    @Override
    public boolean containsObjects(Object region, StorageFilter filter) throws RemoteException {
        filter.setFirstOnly(true);
        return !searchObjects(region, filter).isEmpty();
    }

    @Override
    public boolean containsObjects(Collection<StorageFilter> filters) throws RemoteException {

        StorageFilter last = null;
        for (StorageFilter filter : filters) {
            last = filter;
        }

        if (last != null) {
            last.setFirstOnly(true);
        }

        return !searchObjects(filters).isEmpty();
    }

    @Override
    public int getObjectCount(Object region, StorageFilter filter) throws RemoteException {
        return searchObjects(region, filter).size();
    }

    @Override
    public int getObjectCount(Collection<StorageFilter> filters) throws RemoteException {
        return searchObjects(filters).size();
    }

    // Поддержка сессии

    @Override
    public void put(Object connectionId, Object region, Object key, Object val)
            throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Put: ConnectionId - " + connectionId + "; region - " + region + "; key - " + key);
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        connection.addPutAction(region, key, val);
    }

    @Override
    public void put(Object connectionId, Object region, Map values)
            throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Put map: ConnectionId - " + connectionId + "; region - " + region + "; Map - " + values);
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        connection.addPutAction(region, values);
    }

    @Override
    public void putMap(Object connectionId, Object region, byte[] values)
            throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Put Map: ConnectionId - " + connectionId + "; region - " + region + "; bytes[].");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        connection.addPutAction(region, (Map) ByteConverter.bytesToObject(values));
    }

    @Override
    public Object get(Object connectionId, Object region, Object key) throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Get: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        return connection.get(region, key);
    }

    @Override
    public byte[] getRegionContentAsBytes(Object connectionId, Object region)
            throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Get byte[]: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        return ByteConverter.objectToBytes(connection.get(region));
    }

    @Override
    public Map getRegionContent(Object connectionId, Object region) throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Get Map: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        return connection.get(region);
    }

    @Override
    public void remove(Object connectionId, Object region, Object key)
            throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("Remove: ConnectionId - " + connectionId + "; region - " + region + "; key - " + key);
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        connection.addRemoveAction(region, key);
    }

    @Override
    public Set<Object> getKeys(Object connectionId, Object region) throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("getKeys: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        return connection.getKeys(region);
    }

    @Override
    public boolean hasRegion(Object connectionId, Object region) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("hasRegion: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        return connection.hasRegion(region);
    }

    @Override
    public Set getRegions(Object connectionId) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("getRegions: ConnectionId - " + connectionId + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        return connection.getRegions();
    }

    @Override
    public void createRegion(Object connectionId, Object region) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("createRegion: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        connection.addCreateRegionAction(region);
    }

    @Override
    public void clearRegion(Object connectionId, Object region) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("clearRegion: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        connection.addClearRegionAction(region);
    }

    @Override
    public void removeRegion(Object connectionId, Object region) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("removeRegion: ConnectionId - " + connectionId + "; region - " + region + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        connection.addRemoveRegionAction(region);
    }

    @Override
    public Collection searchObjects(Object connectionId, Object region, StorageFilter filter) throws RemoteException {
        DefaultConnection connection = DefaultConnection.getConnection(connectionId);

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("searchObjects: ConnectionId - " + connectionId + "; region - "
                    + region + "; StorageFilter: " + filter + ".");
        }

        Collection filterResult;
        try {
            filterResult = connection.searchObjects(region, filter);
        } catch (TimeoutException e) {
            throw new RemoteException("Lock timeout", e);
        }

        if (filter.isExtractionRequiered()) {
            filterResult = filter.extractValues(filterResult);
        }

        return filterResult;
    }

    @Override
    public Collection searchObjects(Object connectionId, Collection<StorageFilter> filters) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("searchObjects: ConnectionId - " + connectionId + "; StorageFilters: " + filters + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        try {
            return connection.searchObjects(filters);
        } catch (TimeoutException e) {
            throw new RemoteException("Lock timeout", e);
        }
    }

    @Override
    public boolean containsObjects(Object connectionId, Object region, StorageFilter filter) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("containsObjects: ConnectionId - " + connectionId + "; region - "
                    + region + "; StorageFilter: " + filter + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        try {
            return connection.containsObjects(region, filter);
        } catch (TimeoutException e) {
            throw new RemoteException("Lock timeout", e);
        }
    }

    @Override
    public boolean containsObjects(Object connectionId, Collection<StorageFilter> filters) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("containsObjects: ConnectionId - " + connectionId + "; StorageFilter: " + filters + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        try {
            return connection.containsObjects(filters);
        } catch (TimeoutException e) {
            throw new RemoteException("Lock timeout", e);
        }
    }

    @Override
    public int getObjectCount(Object connectionId, Object region, StorageFilter filter) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("getObjectCount: ConnectionId - " + connectionId + "; region - "
                    + region + "; StorageFilter: " + filter + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        try {
            return connection.getObjectCount(region, filter);
        } catch (TimeoutException e) {
            throw new RemoteException("Lock timeout", e);
        }
    }

    @Override
    public int getObjectCount(Object connectionId, Collection<StorageFilter> filters) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("getObjectCount: ConnectionId - " + connectionId + "; StorageFilters: " + filters + ".");
        }

        DefaultConnection connection = DefaultConnection.getConnection(connectionId);
        try {
            return connection.getObjectCount(filters);
        } catch (TimeoutException e) {
            throw new RemoteException("Lock timeout", e);
        }
    }

    @Override
    public Object createConnection() throws RemoteException {
        return DefaultConnection.getFreeConnectionId();
    }

    //


    @Override
    public void commit(Object connectionId) throws Exception {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, " - commit: ConnectionId - " + connectionId + ".");
        }

        DefaultConnection.getConnection(connectionId).commit();
    }

    @Override
    public void rollback(Object connectionId) throws Exception {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("rollback: ConnectionId - " + connectionId + ".");
        }

        DefaultConnection.getConnection(connectionId).rollback();
    }

    @Override
    public void close(Object connectionId) throws Exception {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("close: ConnectionId - " + connectionId + ".");
        }

        DefaultConnection.getConnection(connectionId).close();
    }

    //

    @Override
    public void lock(Object connectionId, Object region, Object key) throws RemoteException, TimeoutException {
        lock(connectionId, region, key, Lock.LOC_REQUEST_TIMEOUT);
    }

    @Override
    public void lock(Object connectionId, Object region, Object key, long timeOut) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("lock: ConnectionId - " + connectionId + "; region - " + region +
                    "; Key - " + key + "; timeOut - " + timeOut);
        }

        Lock.lock(connectionId, region, key, Lock.LOC_REQUEST_TIMEOUT, false);
    }

    @Override
    public void unlock(Object connectionId, Object region, Object key) throws RemoteException {
        unLock(connectionId, region, key, Lock.LOC_REQUEST_TIMEOUT);
    }

    @Override
    public void unLock(Object connectionId, Object region, Object key, long timeOut) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("unLock: ConnectionId - " + connectionId + "; region - " + region +
                    "; Key - " + key + "; timeOut - " + timeOut);
        }

        Lock.unLock(connectionId, region, key, timeOut, false);
    }
//

    @Override
    public String echo(String message) throws Exception {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("echo Request: msg -  " + message);
        }

        if (message.length() > 100) {
            throw new Exception("To long string, size [" + message.length() + "]");
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("echo Request: response was sent...");
        }

        return message;
    }

}
