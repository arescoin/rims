/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server.admin.commands;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CommandParam.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public abstract class CommandParam {

    private final String name;
    private final boolean required;

    @SuppressWarnings("WeakerAccess")
    protected CommandParam(String name, boolean required) {
        this.name = name;
        this.required = required;
    }

    @SuppressWarnings("unused")
    public abstract Object obtain(String paramValue);

    @SuppressWarnings("WeakerAccess")
    public String[] splitParams(String paramsPart) {
        String[] strings = paramsPart.trim().split("\\s");
        for (String string : strings) {
            string = string.trim();
            if (string.startsWith(getName())) {
                //fixme: ого!!! сколько лет и не стрельнуло!!! :p :)
            }
        }

        return strings;
    }

    @SuppressWarnings("WeakerAccess")
    public String getName() {
        return name;
    }

    @SuppressWarnings("WeakerAccess")
    public boolean isRequired() {
        return required;
    }
}
