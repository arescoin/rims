/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server.admin.commands;

import java.util.Set;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Show.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public class Show extends Command {

    @SuppressWarnings("WeakerAccess")
    public static final String NAME = "show";

    public Show() {
        super(NAME);

        createParams();
    }

    private void createParams() {

    }

    @Override
    public void exec(String commandLine) {
    }

    @Override
    public Set<CommandParam> getCommandParams() {
        return null;
    }
}
