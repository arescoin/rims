/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import ru.xr.cache.RegionNotFoundException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Эмулятор кеша. Используется в сессиях для аккумулирования вносимых изменений.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConnectionsLocalCache.java 75 2011-04-21 11:52:29Z dgomon $"
 */
@SuppressWarnings("WeakerAccess")
class ConnectionsLocalCache {

    /** Карта регионов */
    private final Map<Object, Map<Object, Object>> cache = new ConcurrentHashMap<>();

    /**
     * Размещает объект в указанном регионе по указанному ключу,
     * в случае присутствия там объекта, он будет заменен на новый
     *
     * @param region регион для размещения
     * @param key    ключ для размещения
     * @param val    размещаемое значение
     * @throws ru.xr.cache.RegionNotFoundException при попытке указать несуществующий регион
     */
    public void put(Object region, Object key, Object val) throws RegionNotFoundException {
        checkRegion(region);
        try {
            cache.get(region).put(key, val);
        } catch (NullPointerException e) {
            throw new RegionNotFoundException(region.toString());
        }
    }

    /**
     * Размещает объекты в указанном регионе,
     * в случае присутствия там объекта, он будет заменен на новый
     *
     * @param region регион для размещения
     * @param vals   размещаемое значение
     * @throws ru.xr.cache.RegionNotFoundException при попытке указать несуществующий регион
     */
    @SuppressWarnings({"unchecked"})
    public void putAll(Object region, Map vals) throws RegionNotFoundException {
        checkRegion(region);
        try {
            cache.get(region).putAll(vals);
        } catch (NullPointerException e) {
            throw new RegionNotFoundException(region.toString());
        }
    }

    /**
     * Возвращает объект размещенный в указанном регионе под указанным ключем
     *
     * @param region регион размещения
     * @param key    ключ для получения объекта
     * @return объект размещенный по данным координатам
     * @throws ru.xr.cache.RegionNotFoundException при попытке указать несуществующий регион
     */
    public Object get(Object region, Object key) throws RegionNotFoundException {
        checkRegion(region);
        try {
            return cache.get(region).get(key);
        } catch (NullPointerException e) {
            throw new RegionNotFoundException(region.toString());
        }
    }

    /**
     * Удаляет объект размещенный в указанном регионе под указанным ключем
     *
     * @param region регион
     * @param key    ключ объекта
     * @throws ru.xr.cache.RegionNotFoundException при попытке указать несуществующий регион
     */
    public void remove(Object region, Object key) throws RegionNotFoundException {
        checkRegion(region);
        try {
            cache.get(region).remove(key);
        } catch (NullPointerException e) {
            throw new RegionNotFoundException(region.toString());
        }
    }

    /**
     * Возвращает набор ключей присутствующих в указанном регионе
     *
     * @param region регион
     * @return ключи
     * @throws ru.xr.cache.RegionNotFoundException при попытке указать несуществующий регион
     */
    public Set<Object> getKeys(Object region) throws RegionNotFoundException {
        checkRegion(region);
        try {
            return cache.get(region).keySet();
        } catch (NullPointerException e) {
            throw new RegionNotFoundException(region.toString());
        }
    }

    /**
     * Возвращает признак наличия указанного региона
     *
     * @param region регион
     * @return true - регион существует, иначе - false
     */
    public boolean hasRegion(Object region) {
        return cache.containsKey(region);
    }

    /**
     * Создает в кеше новый регион
     *
     * @param region создаваемый регион
     */
    public void createRegion(Object region) {
        cache.put(region, new HashMap<>());
    }

    /**
     * Очищает все значения в указанном регионе
     *
     * @param region очищаемый регион
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public void clearRegion(Object region) {
        cache.put(region, new HashMap<>());
    }

    /**
     * Возвращает набор содержащий имена существующих с кеше регионов
     *
     * @return регионы
     */
    public Set<Object> getRegions() {
        return cache.keySet();
    }

    /**
     * Удаляет из кеша регион
     *
     * @param region удаляемый регион
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public void removeRegion(Object region) {
        cache.remove(region);
    }

    private void checkRegion(Object region) {
        if (!cache.containsKey(region)) {
            cache.put(region, new HashMap<>());
        }
    }

    /**
     * Возвращает содержимое региона
     *
     * @param region идентификатор региона
     * @return содержимое региона
     * @throws RegionNotFoundException при попытке указать несуществующий регион
     */
    public Map getRegionContent(Object region) throws RegionNotFoundException {
        Map result;
        if (cache.containsKey(region)) {
            result = cache.get(region);
        } else {
            result = new HashMap();
        }

        return result;
    }

}
