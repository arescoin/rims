/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server.admin.commands;

import java.util.*;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Command.java 51 2010-11-01 13:36:29Z dgomon $"
 */
public abstract class Command {

    @SuppressWarnings("WeakerAccess")
    protected static final Map<String, Command> COMMANDS_MAP = new HashMap<>();

    private final String name;
    @SuppressWarnings("WeakerAccess")
    protected Set<CommandParam> commandParams = new HashSet<>();
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final Set<String> requires = new HashSet<>();

    @SuppressWarnings("WeakerAccess")
    protected Command(String name) {
        this.name = name;
        COMMANDS_MAP.put(this.name, this);
    }

    public abstract void exec(String commandLine);

    @SuppressWarnings("WeakerAccess")
    protected void addParam(CommandParam param) {
        commandParams.add(param);

        if (param.isRequired()) {
            requires.add(param.getName());
        }
    }

    public static Command getCommand(String name) {
        return COMMANDS_MAP.get(name);
    }

    public static Set<String> getNames() {
        return Collections.unmodifiableSet(COMMANDS_MAP.keySet());
    }

    @SuppressWarnings("WeakerAccess")
    protected String trimCommandByName(String commandLine) {
        return commandLine.trim().substring(name.length()).trim();
    }

    @SuppressWarnings("unused")
    public Set<CommandParam> getCommandParams() {
        return Collections.unmodifiableSet(commandParams);
    }

    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }


    @SuppressWarnings("unused")
    public String[] getArguments(String source) {
        String[] result = null;

        if (source != null) {
            source = source.trim();
            if (!source.isEmpty()) {
                if (source.contains("-")) {
                    result = source.split("-");

                    for (int i = 0; i < result.length; i++) {
                        result[i] = "-" + result[i];
                    }
                }
            }

        }

        return result;
    }
}
