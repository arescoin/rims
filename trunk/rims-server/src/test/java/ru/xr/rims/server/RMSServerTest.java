/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import junit.framework.Assert;
import org.junit.Test;
import ru.xr.rims.common.Application;
import ru.xr.rims.common.Configuration;
import ru.xr.rims.common.ServerArray;
import ru.xr.rims.services.GlobalService;
import ru.xr.rims.services.TimeoutException;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Set;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSServerTest.java 47 2010-11-01 08:51:40Z dgomon $"
 */
public class RMSServerTest {

    public static final String TEST_CONFIG_PATH = "./servers.properties";
    public static final String NULL_REGION = "NULL_REGION";
    public static final String DEF_KEY = "DEF_KEY";
    public static final String DEF_VAL = "DEF_VAL";

    @org.junit.BeforeClass
    public static void prepare() {

        String filePath = RMSServerTest.class.getResource(TEST_CONFIG_PATH).getFile();
        RMSServer.main(new String[]{Application.Argument.cf.name() + Application.PARAM_DELIMETER + filePath});
    }

    @Test
    public void test() {
        System.out.println("Configuration.isInitialized(): " + Configuration.isInitialized());

        ServerArray.Server server = ServerArray.getArray().getCurrent();
        try {
            server.initRegistry(true);

            GlobalService globalService = server.getRemoteCache();

            testRegions(globalService);
            testRegionObjects(globalService);
            testSessions(globalService);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void testRegions(GlobalService globalService) throws NotBoundException, RemoteException, TimeoutException {
        Set set = globalService.getRegions();
        int startSize = set.size();

        globalService.createRegion(NULL_REGION);
        set = globalService.getRegions();
        int nextSize = set.size();
        Assert.assertTrue("Wrong regions number", nextSize - startSize == 1);

        Assert.assertTrue("Region not found: " + NULL_REGION, globalService.hasRegion(NULL_REGION));
        globalService.removeRegion(NULL_REGION);
        Assert.assertTrue("Region must be deleted: " + NULL_REGION, !globalService.hasRegion(NULL_REGION));
    }

    private void testRegionObjects(GlobalService globalService) throws TimeoutException, RemoteException {
        globalService.createRegion(NULL_REGION);
        globalService.put(NULL_REGION, DEF_KEY, DEF_VAL);
        Assert.assertTrue("Wrong cache value", globalService.get(NULL_REGION, DEF_KEY).equals(DEF_VAL));

        globalService.remove(NULL_REGION, DEF_KEY);
        Assert.assertTrue("Value must bee null", globalService.get(NULL_REGION, DEF_KEY) == null);
        globalService.removeRegion(NULL_REGION);
    }

    private void testSessions(GlobalService globalService) throws Exception {
        globalService.removeRegion(NULL_REGION);
        Object connection = globalService.createConnection();
        globalService.createRegion(connection, NULL_REGION);

        Assert.assertTrue("Region must be deleted: " + NULL_REGION, !globalService.hasRegion(NULL_REGION));
        Assert.assertTrue("Region not found: " + NULL_REGION, globalService.hasRegion(connection, NULL_REGION));

        globalService.commit(connection);
        globalService.close(connection);

        connection = globalService.createConnection();

        Assert.assertTrue("Region not found: " + NULL_REGION, globalService.hasRegion(NULL_REGION));

        globalService.removeRegion(connection, NULL_REGION);

        Assert.assertTrue("Region not found: " + NULL_REGION, globalService.hasRegion(NULL_REGION));
        Assert.assertTrue("Region must be deleted: " + NULL_REGION, !globalService.hasRegion(connection, NULL_REGION));

        globalService.commit(connection);
        Assert.assertTrue("Region must be deleted: " + NULL_REGION, !globalService.hasRegion(NULL_REGION));
        globalService.close(connection);

        globalService.removeRegion(NULL_REGION);
        globalService.createRegion(NULL_REGION);
        connection = globalService.createConnection();

        globalService.put(connection, NULL_REGION, DEF_KEY, DEF_VAL);
        Assert.assertTrue(globalService.get(connection, NULL_REGION, DEF_KEY).equals(DEF_VAL));
        Assert.assertNull(globalService.get(NULL_REGION, DEF_KEY));
        globalService.commit(connection);
        globalService.close(connection);
        Assert.assertTrue(globalService.get(NULL_REGION, DEF_KEY).equals(DEF_VAL));
    }

}
