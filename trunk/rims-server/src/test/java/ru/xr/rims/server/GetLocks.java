/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import ru.xr.rims.common.Application;
import ru.xr.rims.common.ServerArray;
import ru.xr.rims.services.GlobalService;

import java.io.FileNotFoundException;
import java.util.Set;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: GetLocks.java 165 2014-07-10 10:57:16Z DGomon $"
 */
public class GetLocks extends Application {
    public static void main(String[] args) throws FileNotFoundException {

        System.out.println("[" + 1e5 + "]");

        try {
            String filePath = RMSServerTest.class.getResource(RMSServerTest.TEST_CONFIG_PATH).getFile();
            new GetLocks().init(filePath);
            ServerArray.Server server = ServerArray.getArray().getCurrent();
            server.initRegistry();

//            ServerStorage serverStorage = server.getServerCache();
//            Map map = serverStorage.getLocks();

            server.getRemoteCache().echo("Test msg");

            System.out.println("   ");

            GlobalService globalService = server.getRemoteCache();
            Set regs = globalService.getRegions();
//            Iterator iterator = regs.iterator();

            System.out.println("Reg size:  " + regs.size());

//            int count = 0;
//            int size;
//            while (iterator.hasNext()) {
//                Object reg = iterator.next();
//                size = globalService.getKeys(reg).size();
//                System.out.println(reg + " : " + size);
//                count += size;
//            }

//            System.out.println("Fsio!!! [" + count + "]");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
