/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TstMain.java 165 2014-07-10 10:57:16Z DGomon $"
 */
public class TstMain {

    public static void main(String[] args) {

        Map map = new HashMap();

        int objectCount = 2;

        for (int i = 0; i < objectCount; i++) {
            map.put(i, "string-" + i);
        }

        MapContainer container = new MapContainer();
        container.setMap(map);

        try {
//            long start = System.currentTimeMillis();
//            write(container, "objs.bin");
//            long stop = System.currentTimeMillis();

//            long startMap = System.currentTimeMillis();
//            write(map, "map.bin");
//            long stopMap = System.currentTimeMillis();

            long startByte;
            long stopByte = 0;

            long startMapByte;
            long stopMapByte = 0;

            long startByteCh;
            long stopByteCh = 0;

            long startMapByteCh;
            long stopMapByteCh = 0;

            long startMapRead;
            long stopMapRead = 0;

            int count = 900000;

            for (int i = 0; i < count; i++) {

//                System.out.println("iteration: " + i);

                startByte = System.currentTimeMillis();
                writeBytes(container, "objs.b.bin");
                stopByte += System.currentTimeMillis() - startByte;

                startMapByte = System.currentTimeMillis();
                writeBytes(map, "map.b.bin");
                stopMapByte += System.currentTimeMillis() - startMapByte;

                startByteCh = System.currentTimeMillis();
                writeChannel(container, "objs.bch.bin");
                stopByteCh += System.currentTimeMillis() - startByteCh;

                startMapByteCh = System.currentTimeMillis();
                writeChannel(map, "map.bch.bin");
                stopMapByteCh += System.currentTimeMillis() - startMapByteCh;

                startMapRead = System.currentTimeMillis();
                readBytes("map.b.bin");
                stopMapRead += System.currentTimeMillis() - startMapRead;
            }


//            System.out.println("Container: " + (stop - start));
//            System.out.println("ContainerM: " + (stopMap - startMap));
//            System.out.println();

            System.out.println("Container b: " + ((double) stopByte / count));
            System.out.println("Map b: " + ((double) stopMapByte / count));
            System.out.println("Map Read: " + ((double) stopMapRead / count));
            System.out.println();

            System.out.println("Container b ch: " + ((double) stopByteCh / count));
            System.out.println("Map b ch: " + ((double) stopMapByteCh / count));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    private static void writeChannel(Object container, String name) throws IOException {

        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);

        objectOutputStream.writeObject(container);

        ByteBuffer byteBuffer = ByteBuffer.wrap(byteOutputStream.toByteArray());

        FileChannel fileChannel = new RandomAccessFile(name, "rw").getChannel();
        fileChannel.write(byteBuffer);

        objectOutputStream.flush();
        objectOutputStream.close();

        byteOutputStream.flush();
        byteOutputStream.close();

        fileChannel.close();
    }

    private static void writeBytes(Object container, String name) throws IOException {

        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);

        objectOutputStream.writeObject(container);

        FileOutputStream fileOutputStream = new FileOutputStream(name);
        fileOutputStream.write(byteOutputStream.toByteArray());

        fileOutputStream.flush();
        fileOutputStream.close();

        objectOutputStream.flush();
        objectOutputStream.close();

        byteOutputStream.flush();
        byteOutputStream.close();
    }

    private static void readBytes(String name) throws IOException, ClassNotFoundException {

        FileInputStream fileInputStream = new FileInputStream(name);
        byte[] bytes = new byte[fileInputStream.available()];
        fileInputStream.read(bytes);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        objectInputStream.readObject();

        objectInputStream.close();
        byteArrayInputStream.close();
        fileInputStream.close();
    }

    private static void write(Object container, String name) throws IOException {

        OutputStream outputStream = new FileOutputStream(name);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        objectOutputStream.writeObject(container);
        objectOutputStream.flush();
        objectOutputStream.close();

        outputStream.flush();
        outputStream.close();
    }

}
