/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.server;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: MapContainer.java 165 2014-07-10 10:57:16Z DGomon $"
 */
public class MapContainer implements Serializable /*Externalizable */ {

    private Map<Object, Object> map;

    public MapContainer() {
        map = new HashMap<Object, Object>();
    }

    public void setMap(Map<Object, Object> map) {
        this.map.putAll(map);
    }

    public Map<Object, Object> getMap() {
        return this.map;
    }

//    @Override
//    public void writeExternal(ObjectOutput out) throws IOException {
//        out.writeInt(map.size());
//
//        for (Map.Entry entry : map.entrySet()) {
//            out.writeObject(entry.getKey());
//            out.writeObject(entry.getValue());
//        }
//    }
//
//    @Override
//    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//        int count = in.readInt();
//
//        for (int i = 0; i < count; i++) {
//            map.put(in.readObject(), in.readObject());
//        }
//    }
}
