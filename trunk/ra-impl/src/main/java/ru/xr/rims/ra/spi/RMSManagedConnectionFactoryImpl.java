/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.spi;


import ru.xr.rims.common.ServerArray;
import ru.xr.rims.ra.cci.RMSConnectionFactoryImpl;
import ru.xr.rims.ra.cci.RMSConnectionRequestInfo;

import javax.resource.ResourceException;
import javax.resource.spi.*;
import javax.security.auth.Subject;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSManagedConnectionFactoryImpl.java 85 2011-06-02 08:37:24Z dgomon $"
 */
public class RMSManagedConnectionFactoryImpl implements ManagedConnectionFactory, ResourceAdapterAssociation {

    private RMSResourceAdapter resourceAdapter;
    private PrintWriter logWriter;

    /** Имя используемого сервера RiMS */
    private String currentServerName;

    private static Logger logger = Logger.getLogger(RMSManagedConnectionFactoryImpl.class.getName());

    public RMSManagedConnectionFactoryImpl() {
        initRMS();
        try {
            logWriter = new PrintWriter("./RMSManagedConnectionFactory.log");
        } catch (FileNotFoundException e) {
            processException(e);
        }

        logger.fine("RMSManagedConnectionFactoryImpl created");
    }

    /** Инициализирует объекты работы с сервером, получает значение текущего сервера и инициализирует регистри */
    private void initRMS() {
        ServerArray.Server currentServer = ServerArray.getArray().getCurrent();
        currentServerName = currentServer.toString();

        try {
            currentServer.initRegistry(true);
        } catch (RemoteException e) {
            processException(e);
        }
    }

    private void processException(Exception e) {
        logger.log(Level.FINE, e.getMessage());
    }

    @Override
    public void setResourceAdapter(ResourceAdapter resourceAdapter) throws ResourceException {
        if (resourceAdapter == null) {
            throw new ResourceException("ResourceAdapter already defined");
        }
        logger.fine("ResourceAdapter defined");
        this.resourceAdapter = (RMSResourceAdapter) resourceAdapter;
    }

    @Override
    public ResourceAdapter getResourceAdapter() {
        return this.resourceAdapter;
    }

    @Override
    public Object createConnectionFactory(ConnectionManager connectionManager) throws ResourceException {
        return new RMSConnectionFactoryImpl(connectionManager, this);
    }

    @Override
    public Object createConnectionFactory() throws ResourceException {
        return new RMSConnectionFactoryImpl(null, this);
    }

    @Override
    public ManagedConnection createManagedConnection(Subject subject, ConnectionRequestInfo connectionRequestInfo)
            throws ResourceException {

        RMSManagedConnectionImpl managedConnection;
        try {
            managedConnection = new RMSManagedConnectionImpl();
        } catch (Exception e) {
            reInitServer();
            try {
                managedConnection = new RMSManagedConnectionImpl();
            } catch (RemoteException e1) {
                throw new ResourceException(e.getMessage(), e);
            }
        }

        return managedConnection;
    }

    @Override
    public ManagedConnection matchManagedConnections(Set set, Subject subject,
            ConnectionRequestInfo connectionRequestInfo) throws ResourceException {

        // пока просто выбираем первый, без какихнить предпочтений...
        ManagedConnection result = null;
        if (!set.isEmpty()) {

            if (connectionRequestInfo != null) {

                RMSConnectionRequestInfo rmsConnectionRequestInfo = (RMSConnectionRequestInfo) connectionRequestInfo;

                for (Object o : set) {
                    RMSManagedConnectionImpl RMSManagedConnectionImpl = (RMSManagedConnectionImpl) o;
                    Object conId = ((ConnectionWrapper) RMSManagedConnectionImpl.
                            getConnection(subject, connectionRequestInfo)).getId();


                    if (rmsConnectionRequestInfo.getRmsConnectionSpec().getConnectionId().equals(conId)) {
                        return (ManagedConnection) o;
                    }
                }
            }

            result = (ManagedConnection) set.iterator().next();
        }

        if (logger.isLoggable(Level.FINE) && result != null) {
            logger.fine("matchManagedConnections ["
                    + ((ConnectionWrapper) result.getConnection(subject, connectionRequestInfo)).
                    getRmsConnection().getId().toString() + "]");
        }

        return result;
    }

    @Override
    public void setLogWriter(PrintWriter printWriter) throws ResourceException {
        this.logWriter = printWriter;
    }

    @Override
    public PrintWriter getLogWriter() throws ResourceException {
        return this.logWriter;
    }

    // методы переопределены в соответствие со спецификацией

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RMSManagedConnectionFactoryImpl that = (RMSManagedConnectionFactoryImpl) o;

        return !(currentServerName != null ? !currentServerName.equals(that.currentServerName)
                : that.currentServerName != null);
    }

    @Override
    public int hashCode() {
        return currentServerName != null ? currentServerName.hashCode() : 0;
    }

    /** Повторная инициялизация работы с сервером */
    @SuppressWarnings("WeakerAccess")
    void reInitServer() {
        initRMS();
    }

}
