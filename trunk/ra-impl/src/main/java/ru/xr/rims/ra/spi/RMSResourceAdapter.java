/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.spi;

import ru.xr.rims.common.Configuration;

import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.BootstrapContext;
import javax.resource.spi.ResourceAdapter;
import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.transaction.xa.XAResource;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSResourceAdapter.java 67 2011-03-16 11:43:32Z dgomon $"
 */
public class RMSResourceAdapter implements ResourceAdapter, Serializable {

    private String configFile;

    private static Logger logger = Logger.getLogger(RMSResourceAdapter.class.getName());

    @Override
    public void start(BootstrapContext bootstrapContext) throws ResourceAdapterInternalException {
        logger.info("Start RMSResourceAdapter");

        try {
            initConfig(configFile);
        } catch (IOException e) {
            throw new ResourceAdapterInternalException(e.getMessage(), e);
        }
    }

    private void initConfig(String configFileName) throws IOException {
        if (!Configuration.isInitialized()) {
            Configuration.initialize(configFileName);
        } else {
            Configuration.reInit(configFileName);
        }
    }

    @Override
    public void stop() {
        logger.info("Stop RMSResourceAdapter");
    }

    @Override
    public XAResource[] getXAResources(ActivationSpec[] activationSpecs) throws ResourceException {
        return new XAResource[0];
    }

    @Override
    public void endpointActivation(MessageEndpointFactory messageEndpointFactory, ActivationSpec activationSpec)
            throws ResourceException {
    }

    @Override
    public void endpointDeactivation(MessageEndpointFactory messageEndpointFactory, ActivationSpec activationSpec) {
    }

    /**
     * По спеке бинов, для возможности установки параметра сервером приложений
     *
     * @return путь к файлу конфигурации RiMS
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public String getConfigFile() {
        return configFile;
    }

    /**
     * По спеке бинов, для возможности установки параметра сервером приложений
     *
     * @param configFile путь к файлу конфигурации RiMS
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }
}
