/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import javax.resource.ResourceException;
import javax.resource.cci.LocalTransaction;
import java.util.logging.Logger;

/**
 * Заглушка для соблюдения спеки
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSLocalTransactionImpl.java 60 2010-11-15 12:14:01Z dgomon $"
 */
public class RMSLocalTransactionImpl implements LocalTransaction {

    private static Logger logger = Logger.getLogger(RMSLocalTransactionImpl.class.getName());

    @Override
    public void begin() throws ResourceException {
        logger.finest("RMSLocalTransactionImpl.begin");
    }

    @Override
    public void commit() throws ResourceException {
        logger.finest("RMSLocalTransactionImpl.commit");
    }

    @Override
    public void rollback() throws ResourceException {
        logger.finest("RMSLocalTransactionImpl.rollback");
    }
}
