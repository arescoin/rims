/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import ru.xr.rims.ra.Fake;
import ru.xr.rims.ra.spi.RMSManagedConnectionFactoryImpl;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.resource.ResourceException;
import javax.resource.cci.*;
import javax.resource.spi.ConnectionManager;
import java.rmi.RemoteException;

/**
 * Реализация {@link ConnectionFactory}, для подключения к RiMS
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSConnectionFactoryImpl.java 77 2011-05-06 08:02:19Z dgomon $"
 */
public class RMSConnectionFactoryImpl implements ConnectionFactory {

    /** Переменная для соблюдения спеки */
    private Reference reference;

    // Устанавливаются в конструкторе
    private final ConnectionManager connectionManager;
    private final RMSManagedConnectionFactoryImpl managedConnectionFactory;

    public RMSConnectionFactoryImpl(ConnectionManager connectionManager,
            RMSManagedConnectionFactoryImpl managedConnectionFactory) {

        this.connectionManager = connectionManager;
        this.managedConnectionFactory = managedConnectionFactory;
    }

    @Override
    public Connection getConnection() throws ResourceException {
        if (connectionManager == null) {
            // это нештетная ситуация!!!
            try {
                return new RMSConnectionImpl();
            } catch (RemoteException e) {
                throw new ResourceException(e.getMessage(), e);
            }
        }

        // а так по правилам
        return (Connection) connectionManager.allocateConnection(managedConnectionFactory, null);
    }

    @Override
    public Connection getConnection(ConnectionSpec connectionSpec) throws ResourceException {
        if (connectionManager == null) {
            // это нештетная ситуация!!!
            return getConnection();
        }

        return (Connection) connectionManager.allocateConnection(managedConnectionFactory,
                new RMSConnectionRequestInfo((RMSConnectionSpec) connectionSpec));
    }

    /** Возвращает заглушку для соблюдения спеки */
    @Override
    public RecordFactory getRecordFactory() throws ResourceException {
        return new RMSRecordFactoryImpl();
    }

    /** Возвращает заглушку для соблюдения спеки */
    @Override
    public ResourceAdapterMetaData getMetaData() throws ResourceException {
        return Fake.RESOURCE_ADAPTER_META_DATA;
    }

    @Override
    public void setReference(Reference reference) {
        this.reference = reference;
    }

    @Override
    public Reference getReference() throws NamingException {
        return this.reference;
    }
}
