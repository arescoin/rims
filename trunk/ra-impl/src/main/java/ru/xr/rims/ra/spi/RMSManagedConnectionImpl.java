/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.spi;

import ru.xr.rims.ra.Fake;
import ru.xr.rims.ra.cci.RMSConnectionImpl;

import javax.resource.ResourceException;
import javax.resource.spi.*;
import javax.security.auth.Subject;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;
import java.io.PrintWriter;
import java.lang.IllegalStateException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Обертка для управления подключениями к RiMS средствами AP-сервера
 * <p/>
 * Содержит ссылку на {@link ConnectionWrapper}, черезкоторый происходит взаимодействие с RIMS-сервером
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSManagedConnectionImpl.java 85 2011-06-02 08:37:24Z dgomon $"
 */
public class RMSManagedConnectionImpl implements ManagedConnection {

    private static final Logger logger = Logger.getLogger(RMSManagedConnectionImpl.class.getName());

    /** содержит карту подключений к ХА-транзакциям */
    private static final Map<ArrayList<Byte>, TransactionContainer> CONNECTIONS =
            new HashMap<>();

    /** обертка для использования в локальных транзакциях */
    private final RMSLocalTransactionSPIIml localTransaction = new RMSLocalTransactionSPIIml();

    /** Содержит слушатели событий на подключении регистрируемые сервером и не только */
    private final Set<ConnectionEventListener> eventListeners =
            Collections.synchronizedSet(new HashSet<ConnectionEventListener>());

    /** фасад для подключения */
    private ConnectionWrapper connectionWrapper;

    /** представление подключений используемое в XA-транзакциях */
    private RMSXAResource xaResource;

    /** локальный признак пригодности коннекции */
    private boolean destroyed = false;

    private PrintWriter logWriter;

    /**
     * Конструирет новый инстанс
     *
     * @throws java.rmi.RemoteException при ошибке в процессе подключения к RiMS
     */
    @SuppressWarnings("WeakerAccess")
    public RMSManagedConnectionImpl() throws RemoteException {
        this.eventListeners.add(new ServerErrorListener());
        this.connectionWrapper = new ConnectionWrapper(new RMSConnectionImpl(), this);
        this.xaResource = new RMSXAResource();
    }

    /**
     * При повторном открытии транзакции перепривязывает ConnectionWrapper на используемый в транзакции,
     * если это первая транзакция, то сохраняет мапинг Xid-ConnectionWrapper
     *
     * @param xid идентификаток глобальной транзакции
     */
    private synchronized void associate(Xid xid) {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "XID GlobalTransactionId: {0}; XID BranchQualifier: {1}", new Object[]{
                    Arrays.toString(xid.getGlobalTransactionId()), Arrays.toString(xid.getBranchQualifier())});
        }

        final ArrayList<Byte> gtidList = convertIdToList(xid.getGlobalTransactionId());
        final ArrayList<Byte> bq = convertIdToList(xid.getBranchQualifier());

        if (CONNECTIONS.containsKey(gtidList)) {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Associate with exist: {0}", connectionWrapper.getId().toString());
            }

            synchronized (CONNECTIONS) {
                CONNECTIONS.get(gtidList).attache(bq, connectionWrapper);
                connectionWrapper.changeRMSConnection(CONNECTIONS.get(gtidList).getBaseWrapper().getRmsConnection());
            }

        } else {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "new association: {0}", connectionWrapper.getId().toString());
            }

            synchronized (CONNECTIONS) {
                CONNECTIONS.put(gtidList, new TransactionContainer(gtidList, bq, connectionWrapper));
            }
        }
    }


    private ArrayList<Byte> convertIdToList(byte[] bytes) {

        if (bytes == null) {
            throw new NullPointerException("Bytes array can't be null");
        }

        final ArrayList<Byte> byteArrayList = new ArrayList<>();
        for (byte aByte : bytes) {
            byteArrayList.add(aByte);
        }

        return byteArrayList;
    }

    private synchronized void revert(Xid xid) {

        final byte[] globalTransactionId = xid.getGlobalTransactionId();
        final byte[] branchQualifier = xid.getBranchQualifier();

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Xid getGlobalTransactionId: {0}; Xid BranchQualifier: {1}", new Object[]{
                    Arrays.toString(globalTransactionId), Arrays.toString(branchQualifier)});
        }

        //todo: null pointer
        final ArrayList<Byte> key = convertIdToList(globalTransactionId);
        final ArrayList<Byte> bq = convertIdToList(branchQualifier);

        final TransactionContainer transactionContainer = CONNECTIONS.get(key);
        final TransactionWrapper transactionWrapper = transactionContainer.getTransactoinWrapper(bq);
        final ConnectionWrapper cw = transactionWrapper.getConnectionWrapper();

        cw.revertRMSConnection();
    }


    @Override
    public Object getConnection(Subject subject, ConnectionRequestInfo connectionRequestInfo)
            throws ResourceException {

        if (connectionWrapper == null) {
            throw new ResourceException("The connection is not initialized");
        }

        return connectionWrapper;
    }

    @Override
    public void destroy() throws ResourceException {
        if (!destroyed) {
            connectionWrapper.revertRMSConnection();
            connectionWrapper.destroy();
            destroyed = true;
        }
    }

    @Override
    public void cleanup() throws ResourceException {
        destroyed = false;
    }

    @Override
    public void associateConnection(Object o) throws ResourceException {
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("associateConnection: " + o);
        }
    }

    @Override
    public void addConnectionEventListener(ConnectionEventListener connectionEventListener) {
        this.eventListeners.add(connectionEventListener);
    }

    @Override
    public void removeConnectionEventListener(ConnectionEventListener connectionEventListener) {
        this.eventListeners.remove(connectionEventListener);
    }

    @Override
    public XAResource getXAResource() throws ResourceException {
        return this.xaResource;
    }

    @Override
    public LocalTransaction getLocalTransaction() throws ResourceException {
        return localTransaction;
    }

    @Override
    public ManagedConnectionMetaData getMetaData() throws ResourceException {
        return Fake.MANAGED_CONNECTION_META_DATA;
    }

    @Override
    public void setLogWriter(PrintWriter printWriter) throws ResourceException {
        this.logWriter = printWriter;
    }

    @Override
    public PrintWriter getLogWriter() throws ResourceException {
        return this.logWriter;
    }

    void notifyListeners(int eventTypeCode, Exception e) {

        ConnectionEvent event;

        if (e == null) {
            event = new ConnectionEvent(this, eventTypeCode);
        } else {
            event = new ConnectionEvent(this, eventTypeCode, e);
        }

        final HashSet<ConnectionEventListener> listeners = new HashSet<>(eventListeners);

        switch (eventTypeCode) {
        case ConnectionEvent.CONNECTION_CLOSED:
            for (ConnectionEventListener eventListener : listeners) {
                eventListener.connectionClosed(event);
            }
            break;

        case ConnectionEvent.CONNECTION_ERROR_OCCURRED:
            for (ConnectionEventListener eventListener : listeners) {
                eventListener.connectionErrorOccurred(event);
            }
            break;

        case ConnectionEvent.LOCAL_TRANSACTION_STARTED:
            for (ConnectionEventListener eventListener : listeners) {
                eventListener.localTransactionStarted(event);
            }
            break;

        case ConnectionEvent.LOCAL_TRANSACTION_COMMITTED:
            for (ConnectionEventListener eventListener : listeners) {
                eventListener.localTransactionCommitted(event);
            }
            break;

        case ConnectionEvent.LOCAL_TRANSACTION_ROLLEDBACK:
            for (ConnectionEventListener eventListener : listeners) {
                eventListener.localTransactionRolledback(event);
            }
            break;

        default:
            throw new IllegalStateException("Unknown event type [" + eventTypeCode + "]");
        }

    }

    private static String convertFlag(int flag) {
        switch (flag) {
        case XAResource.TMENDRSCAN:
            return "TMENDRSCAN";
        case XAResource.TMFAIL:
            return "TMFAIL";
        case XAResource.TMJOIN:
            return "TMJOIN";
        case XAResource.TMNOFLAGS:
            return "TMNOFLAGS";
        case XAResource.TMONEPHASE:
            return "TMONEPHASE";
        case XAResource.TMRESUME:
            return "TMRESUME";
        case XAResource.TMSTARTRSCAN:
            return "TMSTARTRSCAN";
        case XAResource.TMSUCCESS:
            return "TMENDRSCAN";
        case XAResource.TMSUSPEND:
            return "TMSUSPEND";
        default:
            return "XZ";
        }
    }


    private class RMSLocalTransactionSPIIml implements LocalTransaction {

        @Override
        public void begin() throws ResourceException {

            logger.log(Level.FINE, "RiMS Local transaction begin");

            RMSManagedConnectionImpl.this.notifyListeners(ConnectionEvent.LOCAL_TRANSACTION_STARTED, null);
        }

        @Override
        public void commit() throws ResourceException {

            logger.log(Level.FINE, "RiMS Local transaction commit started");

            try {
                connectionWrapper.commit();
            } catch (Exception e) {
                throw new ResourceException(e.getMessage(), e);
            }

            RMSManagedConnectionImpl.this.notifyListeners(ConnectionEvent.LOCAL_TRANSACTION_COMMITTED, null);

            logger.log(Level.FINE, "RiMS Local transaction committed");
        }

        @Override
        public void rollback() throws ResourceException {

            logger.log(Level.FINE, "RiMS Local transaction rollback begin");

            try {
                connectionWrapper.rollback();
            } catch (Exception e) {
                throw new ResourceException(e.getMessage(), e);
            }

            RMSManagedConnectionImpl.this.notifyListeners(ConnectionEvent.LOCAL_TRANSACTION_ROLLEDBACK, null);

            logger.fine("RiMS Local transaction end");
        }

    } // RMSLocalTransactionSPIIml


    /** слушатель событий на коннекции, обрабатывает ошибки */
    private class ServerErrorListener implements ConnectionEventListener {

        @SuppressWarnings({"ThrowableResultOfMethodCallIgnored"})
        @Override
        public void connectionErrorOccurred(ConnectionEvent connectionEvent) {

            logger.log(Level.WARNING, "Connection Error Occurred [{0}]", connectionEvent.getId());

            final Exception exception = connectionEvent.getException();
            if (exception != null) {
                logger.info("Exception: " + exception.getMessage());
            }

            try {
                connectionWrapper.destroy();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }

        @Override
        public void connectionClosed(ConnectionEvent connectionEvent) {
            logger.log(Level.FINE, "RMSManagedConnectionImpl closed");
        }

        @Override
        public void localTransactionStarted(ConnectionEvent connectionEvent) {
            logger.log(Level.FINE, "RMSManagedConnectionImpl local transaction started");
        }

        @Override
        public void localTransactionCommitted(ConnectionEvent connectionEvent) {
            logger.log(Level.FINE, "RMSManagedConnectionImpl local transaction committed");
        }

        @Override
        public void localTransactionRolledback(ConnectionEvent connectionEvent) {
            logger.log(Level.FINE, "RMSManagedConnectionImpl local transaction Rolled Back");
        }

    } // ServerErrorListener


    /** Частичная реализация для поддержки XA-транзакции */
    private class RMSXAResource implements XAResource {


        /** фейковый таймаут */
        private int transactionTimeout = 1;

        @Override
        public void start(Xid xid, int flag) throws XAException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Start XA: flag[{0}], GlobalTransactionId {1}, BranchQualifier {2}",
                        new Object[]{
                                convertFlag(flag),
                                Arrays.toString(xid.getGlobalTransactionId()),
                                Arrays.toString(xid.getBranchQualifier())
                        }
                );
            }

            try {

                switch (flag) {
                case TMNOFLAGS:
                    associate(xid);
                    break;
                case TMJOIN:
                    associate(xid);
                    break;
                case TMSUSPEND:
                    throw new XAException(XAException.XA_RBCOMMFAIL);
                case TMRESUME:
                    throw new XAException(XAException.XA_RBEND);
                }

            } catch (Exception e) {
                throw new XAException(e.getMessage());
            }
        }

        @Override
        public void commit(Xid xid, boolean onePhase) throws XAException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.INFO, "Commit XA: OnePhase[{0}]; Xid GlobalTransactionId {1} BranchQualifier {2}",
                        new Object[]{onePhase, Arrays.toString(xid.getGlobalTransactionId()),
                                Arrays.toString(xid.getBranchQualifier())});
            }

            try {

                if (onePhase) {
                    CONNECTIONS.get(convertIdToList(xid.getGlobalTransactionId())).commit();
                } else {
                    CONNECTIONS.get(convertIdToList(xid.getGlobalTransactionId())).
                            commit(convertIdToList(xid.getBranchQualifier()));
                }

            } catch (Exception e) {
                throw new XAException(e.getMessage());
            }
        }

        @Override
        public void rollback(Xid xid) throws XAException {

            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, "Rollback XA: GlobalTransactionId {0}, BranchQualifier {1}", new Object[]{
                        Arrays.toString(xid.getGlobalTransactionId()), Arrays.toString(xid.getBranchQualifier())
                });
            }

            try {
                CONNECTIONS.get(convertIdToList(xid.getGlobalTransactionId())).rollback();
            } catch (Exception e) {
                throw new XAException(e.getMessage());
            }
        }

        @Override
        public void end(Xid xid, int flag) throws XAException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.INFO, "End XA:  flag:[{0}], GlobalTransactionId {1}, BranchQualifier {2}",
                        new Object[]{convertFlag(flag), Arrays.toString(xid.getGlobalTransactionId()),
                                Arrays.toString(xid.getBranchQualifier())});
            }

            revert(xid);
        }

        @Override
        public boolean isSameRM(XAResource xaResource) throws XAException {
            return xaResource == this;
        }

        @Override
        public void forget(Xid xid) throws XAException {
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Forget XA: GlobalTransactionId {0}, BranchQualifier {1}", new Object[]{
                        Arrays.toString(xid.getGlobalTransactionId()), Arrays.toString(xid.getBranchQualifier())
                });
            }
        }

        @Override
        public Xid[] recover(int i) throws XAException {
            return new Xid[0];
        }

        @Override
        public int prepare(Xid xid) throws XAException {
            return XA_OK;
        }

        @Override
        public int getTransactionTimeout() throws XAException {
            return transactionTimeout;
        }

        @Override
        public boolean setTransactionTimeout(int i) throws XAException {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "setTransactionTimeout: Timeout[{0}]", i);
            }

            this.transactionTimeout = i;

            return true;
        }

    } // RMSXAResource


    private static class TransactionContainer {

        private final ArrayList<Byte> gtidList;
        private final ConnectionWrapper connectionWrapper;
        private final Map<ArrayList<Byte>, TransactionWrapper> map = new HashMap<>();

        private boolean commited = false;

        private TransactionContainer(ArrayList<Byte> gtidList, ArrayList<Byte> bq, ConnectionWrapper connWrapper) {

            this.gtidList = gtidList;
            this.connectionWrapper = connWrapper;

            attache(bq, connWrapper);
        }

        @SuppressWarnings("WeakerAccess")
        public void rollback() throws Exception {
            connectionWrapper.rollback();
            CONNECTIONS.remove(gtidList);
        }

        @SuppressWarnings("WeakerAccess")
        public void commit() throws Exception {
            connectionWrapper.commit();
            CONNECTIONS.remove(gtidList);
        }

        @SuppressWarnings("WeakerAccess")
        public void commit(ArrayList<Byte> bq) throws Exception {

            if (!commited) {
                if (map.get(bq).getConnectionWrapper() == connectionWrapper) {
                    connectionWrapper.commit();
                    commited = true;
                }
            }

            map.remove(bq);

            if (map.isEmpty()) {
                CONNECTIONS.remove(gtidList);
            }
        }


        @SuppressWarnings({"UnusedDeclaration"})
        public ArrayList<Byte> getGtidList() {
            return gtidList;
        }

        @SuppressWarnings("WeakerAccess")
        public ConnectionWrapper getBaseWrapper() {
            return connectionWrapper;
        }

        TransactionWrapper getTransactoinWrapper(ArrayList<Byte> bq) {
            return map.get(bq);
        }

        void attache(ArrayList<Byte> bq, ConnectionWrapper connWrp) {
            map.put(bq, new TransactionWrapper(bq, connWrp));
        }
    }


    private static class TransactionWrapper {

        private final ArrayList<Byte> bq;
        private final ConnectionWrapper connectionWrapper;

        private TransactionWrapper(ArrayList<Byte> bq, ConnectionWrapper connectionWrapper) {
            this.bq = bq;
            this.connectionWrapper = connectionWrapper;
        }

        @SuppressWarnings({"UnusedDeclaration"})
        public ArrayList<Byte> getBq() {
            return bq;
        }

        @SuppressWarnings("WeakerAccess")
        public ConnectionWrapper getConnectionWrapper() {
            return connectionWrapper;
        }
    }


}
