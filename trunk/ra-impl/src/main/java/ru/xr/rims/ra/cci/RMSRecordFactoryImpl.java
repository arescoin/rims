/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import javax.resource.ResourceException;
import javax.resource.cci.IndexedRecord;
import javax.resource.cci.MappedRecord;
import javax.resource.cci.RecordFactory;

/**
 * Заглушка для соблюдения спеки
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSRecordFactoryImpl.java 53 2010-11-02 13:06:29Z dgomon $"
 */
public class RMSRecordFactoryImpl implements RecordFactory {
    @Override
    public MappedRecord createMappedRecord(String s) throws ResourceException {
        return null;
    }

    @Override
    public IndexedRecord createIndexedRecord(String s) throws ResourceException {
        return null;
    }
}
