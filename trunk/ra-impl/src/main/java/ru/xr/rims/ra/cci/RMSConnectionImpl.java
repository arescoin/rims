/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import ru.xr.cache.RegionNotFoundException;
import ru.xr.rims.common.ServerArray;
import ru.xr.rims.ra.Fake;
import ru.xr.rims.services.ConnectionSupport;
import ru.xr.rims.services.StorageFilter;
import ru.xr.rims.services.TimeoutException;

import javax.resource.NotSupportedException;
import javax.resource.ResourceException;
import javax.resource.cci.ConnectionMetaData;
import javax.resource.cci.Interaction;
import javax.resource.cci.LocalTransaction;
import javax.resource.cci.ResultSetInfo;
import javax.resource.spi.ConnectionEventListener;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Реализация интерфейса для подключения к RMS
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSConnectionImpl.java 85 2011-06-02 08:37:24Z dgomon $"
 */
public class RMSConnectionImpl implements RMSConnection {

    /** Прокси на удаленный сервис */
    private ConnectionSupport remoteStorage;

    /** Идентификатор подключения (сессии) к RiMS (генерируется на стороне RiMS) */
    private Object serverConnectionId;

    private static Logger logger = Logger.getLogger(RMSConnectionImpl.class.getName());

    /**
     * Конструирует новое подключение
     * <p/>
     * remoteStorage удаленный сервис RiMS
     *
     * @throws java.rmi.RemoteException В сдучае ошибки при создании сессии
     */
    public RMSConnectionImpl() throws RemoteException {
        initConnection();
    }

    private void initConnection() throws RemoteException {

        try {
            this.remoteStorage = ServerArray.getArray().getCurrent().getRemoteCache();
            this.serverConnectionId = this.remoteStorage.createConnection();
        } catch (NotBoundException e) {
            throw new RemoteException(e.getMessage(), e);
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "New RMSConnectionImpl instance created [{0}]",
                    this.serverConnectionId.toString());
        }
    }

    @Override
    public Object getId() {
        return serverConnectionId;
    }

    /** не используется */
    public Interaction createInteraction() throws ResourceException {
        return null;
    }

    public LocalTransaction getLocalTransaction() throws ResourceException {
        return new RMSLocalTransactionImpl();
    }

    /** всегда возвращает одно и тоже */
    public ConnectionMetaData getMetaData() throws ResourceException {
        return Fake.RMS_CONNECTION_META_DATA;
    }

    public void close() throws ResourceException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Real connection closed [{0}]", this.serverConnectionId.toString());
        }

        // освобождает идентификатор предоставленный RiMS-сервером
        try {
            remoteStorage.close(serverConnectionId);
        } catch (Exception e) {
            throw new ResourceException(e.getMessage(), e);
        }
    }

    public ResultSetInfo getResultSetInfo() throws ResourceException {
        throw new NotSupportedException();
    }

    //далее следует работа с кешом...

    public void put(Object region, Object key, Object val)
            throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Put object: connId[{0}], Region[{1}], Key[{2}], Val[{3}]",
                    new Object[]{this.serverConnectionId.toString(), region, key, val});
        }

        try {
            this.remoteStorage.put(serverConnectionId, region, key, val);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.put(serverConnectionId, region, key, val);
        }
    }

    @Override
    public void put(Object region, Map values) throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Put map: connId[{0}], Region[{1}], Vals[{2}]",
                    new Object[]{this.serverConnectionId.toString(), region, values});
        }

        try {
            this.remoteStorage.put(serverConnectionId, region, values);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.put(serverConnectionId, region, values);
        }
    }

    @Override
    public void putMap(Object region, byte[] values) throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Put map [bytes]: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            this.remoteStorage.putMap(serverConnectionId, region, values);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.putMap(serverConnectionId, region, values);
        }
    }

    public Object get(Object region, Object key) throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Get: connId[{0}], Region[{1}], Key[{2}].",
                    new Object[]{this.serverConnectionId.toString(), region, key});
        }

        try {
            return this.remoteStorage.get(serverConnectionId, region, key);
        } catch (ConnectException e) {
            initConnection();
            return this.remoteStorage.get(serverConnectionId, region, key);
        }
    }

    @Override
    public Map getRegionContent(Object region) throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Get Region Content: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            return this.remoteStorage.getRegionContent(serverConnectionId, region);
        } catch (ConnectException e) {
            initConnection();
            return this.remoteStorage.getRegionContent(serverConnectionId, region);
        }
    }

    @Override
    public byte[] getRegionContentAsBytes(Object region) throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Get Region Content As Bytes: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            return this.remoteStorage.getRegionContentAsBytes(serverConnectionId, region);
        } catch (ConnectException e) {
            initConnection();
            return this.remoteStorage.getRegionContentAsBytes(serverConnectionId, region);
        }
    }

    public void remove(Object region, Object key) throws RemoteException, RegionNotFoundException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Remove: connId[{0}], Region[{1}], Key[{2}].",
                    new Object[]{this.serverConnectionId.toString(), region, key});
        }

        try {
            this.remoteStorage.remove(serverConnectionId, region, key);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.remove(serverConnectionId, region, key);
        }
    }

    public Set<Object> getKeys(Object region) throws RemoteException, RegionNotFoundException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Get keys: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            return this.remoteStorage.getKeys(serverConnectionId, region);
        } catch (ConnectException e) {
            initConnection();
            return this.remoteStorage.getKeys(serverConnectionId, region);
        }
    }

    public boolean hasRegion(Object region) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "HasRegion: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            return this.remoteStorage.hasRegion(serverConnectionId, region);
        } catch (ConnectException e) {
            initConnection();
            return this.remoteStorage.hasRegion(serverConnectionId, region);
        }
    }

    public void createRegion(Object region) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "CreateRegion: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            this.remoteStorage.createRegion(serverConnectionId, region);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.createRegion(serverConnectionId, region);
        }
    }

    public void clearRegion(Object region) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "ClearRegion: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            this.remoteStorage.clearRegion(serverConnectionId, region);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.clearRegion(serverConnectionId, region);
        }
    }

    public Set getRegions() throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "GetRegions: connId[{0}].", this.serverConnectionId.toString());
        }

        try {
            return this.remoteStorage.getRegions(serverConnectionId);
        } catch (ConnectException e) {
            initConnection();
            return this.remoteStorage.getRegions(serverConnectionId);
        }
    }

    public void removeRegion(Object region) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "RemoveRegion: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            this.remoteStorage.removeRegion(serverConnectionId, region);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.removeRegion(serverConnectionId, region);
        }
    }

    @Override
    public Collection searchObjects(Object region, StorageFilter filter) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "SearchObjects: connId[{0}], Region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            return remoteStorage.searchObjects(serverConnectionId, region, filter);
        } catch (ConnectException e) {
            initConnection();
            return remoteStorage.searchObjects(serverConnectionId, region, filter);
        }
    }

    @Override
    public Collection searchObjects(Collection<StorageFilter> filters) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "SearchObjects: connId[{0}].", this.serverConnectionId.toString());
        }

        try {
            return remoteStorage.searchObjects(serverConnectionId, filters);
        } catch (ConnectException e) {
            initConnection();
            return remoteStorage.searchObjects(serverConnectionId, filters);
        }
    }

    @Override
    public boolean containsObjects(Object region, StorageFilter filter) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "ContainsObjects: connId[{0}], region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            return remoteStorage.containsObjects(serverConnectionId, region, filter);
        } catch (ConnectException e) {
            initConnection();
            return remoteStorage.containsObjects(serverConnectionId, region, filter);
        }
    }

    @Override
    public boolean containsObjects(Collection<StorageFilter> filters) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "ContainsObjects: connId[{0}].", this.serverConnectionId.toString());
        }

        try {
            return remoteStorage.containsObjects(serverConnectionId, filters);
        } catch (ConnectException e) {
            initConnection();
            return remoteStorage.containsObjects(serverConnectionId, filters);
        }
    }

    @Override
    public int getObjectCount(Object region, StorageFilter filter) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "GetObjectCount: connId[{0}], region[{1}].",
                    new Object[]{this.serverConnectionId.toString(), region});
        }

        try {
            return remoteStorage.getObjectCount(serverConnectionId, region, filter);
        } catch (ConnectException e) {
            initConnection();
            return remoteStorage.getObjectCount(serverConnectionId, region, filter);
        }
    }

    @Override
    public int getObjectCount(Collection<StorageFilter> filters) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "GetObjectCount: connId[{0}].", this.serverConnectionId.toString());
        }

        try {
            return remoteStorage.getObjectCount(serverConnectionId, filters);
        } catch (ConnectException e) {
            initConnection();
            return remoteStorage.getObjectCount(serverConnectionId, filters);
        }
    }

    public void lock(Object region, Object key) throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Lock: connId[{0}], Region[{1}], Key[{2}].",
                    new Object[]{this.serverConnectionId.toString(), region, key});
        }

        try {
            this.remoteStorage.lock(serverConnectionId, region, key);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.lock(serverConnectionId, region, key);
        }
    }

    public void unlock(Object region, Object key) throws RemoteException {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "unLock: connId[{0}], Region[{1}], Key[{2}].",
                    new Object[]{this.serverConnectionId.toString(), region, key});
        }

        try {
            this.remoteStorage.unlock(serverConnectionId, region, key);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.unlock(serverConnectionId, region, key);
        }
    }

    /**
     * Непосредственно инициирует комит на стороне RiMS-сервера
     *
     * @throws Exception при возникновении ошибки на стороне RiMS-сервера
     */
    public void commit() throws Exception {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Commit: connId[{0}].", this.serverConnectionId.toString());
        }

        try {
            this.remoteStorage.commit(serverConnectionId);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.commit(serverConnectionId);
        }
    }

    /**
     * Непосредственно инициирует откат на стороне RiMS-сервера
     *
     * @throws Exception при возникновении ошибки на стороне RiMS-сервера
     */
    public void rollback() throws Exception {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Rollback: connId[{0}].", this.serverConnectionId.toString());
        }

        try {
            this.remoteStorage.rollback(serverConnectionId);
        } catch (ConnectException e) {
            initConnection();
            this.remoteStorage.rollback(serverConnectionId);
        }
    }

    @Override
    public void setInUse() {
    }

    @Override
    public boolean isInUse() {
        return false;
    }

    @Override
    public void addCloseTrigger(ConnectionEventListener listener) {
    }

    @Override
    public void removeCloseTrigger(ConnectionEventListener listener) {
    }
}
