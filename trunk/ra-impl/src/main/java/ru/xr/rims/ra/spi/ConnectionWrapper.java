/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.spi;

import ru.xr.cache.RegionNotFoundException;
import ru.xr.rims.ra.cci.RMSConnection;
import ru.xr.rims.ra.cci.RMSConnectionImpl;
import ru.xr.rims.services.StorageFilter;
import ru.xr.rims.services.TimeoutException;

import javax.resource.ResourceException;
import javax.resource.cci.ConnectionMetaData;
import javax.resource.cci.Interaction;
import javax.resource.cci.LocalTransaction;
import javax.resource.cci.ResultSetInfo;
import javax.resource.spi.ConnectionEvent;
import javax.resource.spi.ConnectionEventListener;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Обертка для подключения к RiMS, прозрачно для клиентской части закрывает соединения после обращения к кешу,
 * ведет учет использования соединения при вложенных обращениях к кешу
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConnectionWrapper.java 85 2011-06-02 08:37:24Z dgomon $"
 */
public class ConnectionWrapper implements RMSConnection {

    /** */
    private RMSConnectionImpl rmsConnection;
    private RMSConnectionImpl oldRmsConnection;

    private RMSManagedConnectionImpl managedConnection;

    /** Счетчик использования объекта */
    private int inUse = 0;

    private static Logger logger = Logger.getLogger(ConnectionWrapper.class.getName());


    @SuppressWarnings("WeakerAccess")
    public ConnectionWrapper(RMSConnectionImpl rmsConnection, RMSManagedConnectionImpl managedConnection) {

        if (rmsConnection == null) {
            throw new IllegalArgumentException("Physical connection can't be null");
        }

        this.rmsConnection = rmsConnection;
        this.oldRmsConnection = rmsConnection;

        this.managedConnection = managedConnection;
    }

    /**
     * Подменяет используемый RMSConnectionImpl на переданный, вернуть
     * в исходное состояние можно методом {@link #revertRMSConnection()}
     *
     * @param rmsConnection подключение которое необходимо использовать
     */
    synchronized void changeRMSConnection(RMSConnectionImpl rmsConnection) {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Connection changing: from[{0}], to[{1}]",
                    new Object[]{this.rmsConnection.getId().toString(), rmsConnection.getId().toString()});
        }

        this.rmsConnection = rmsConnection;
    }

    /** Если была произведена замена подключения, востановит на исходное */
    synchronized void revertRMSConnection() {

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Revert connection: [{0}] to [{1}]",
                    new Object[]{this.rmsConnection.getId().toString(), this.oldRmsConnection.getId().toString()});
        }

        this.rmsConnection = this.oldRmsConnection;
    }

    RMSConnectionImpl getRmsConnection() {
        return rmsConnection;
    }

    @Override
    public Object getId() {
        return rmsConnection.getId();
    }

    @Override
    public Interaction createInteraction() throws ResourceException {
        return this.rmsConnection.createInteraction();
    }

    @Override
    public LocalTransaction getLocalTransaction() throws ResourceException {
        return this.rmsConnection.getLocalTransaction();
    }

    @Override
    public ConnectionMetaData getMetaData() throws ResourceException {
        return this.rmsConnection.getMetaData();
    }

    @Override
    public ResultSetInfo getResultSetInfo() throws ResourceException {
        return this.rmsConnection.getResultSetInfo();
    }

    @Override
    public void close() throws ResourceException {
        // вычитает из счетчика единицу и если более коннекшен не используется, inUse < 1,
        // проходит по листенерам, которые возвращают коннекшен обратно в пул сервера приложений
        inUse--;
        if (inUse == 0) {
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Close wrapper called id[{0}]", this.rmsConnection.getId().toString());
            }

            managedConnection.notifyListeners(ConnectionEvent.CONNECTION_CLOSED, null);

        }

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Wrapper decrement usage [{0}], id[{1}]",
                    new Object[]{inUse, this.rmsConnection.getId().toString()});
        }
    }

    @Override
    public void put(Object region, Object key, Object val) throws RemoteException, RegionNotFoundException, TimeoutException {
        try {
            this.setInUse();
            this.rmsConnection.put(region, key, val);
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    @Override
    public void put(Object region, Map values) throws RemoteException, RegionNotFoundException, TimeoutException {

        try {
            this.setInUse();
            this.rmsConnection.put(region, values);
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    @Override
    public void putMap(Object region, byte[] values)
            throws RemoteException, RegionNotFoundException, TimeoutException {

        try {
            this.setInUse();
            this.rmsConnection.putMap(region, values);
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    @Override
    public Object get(Object region, Object key) throws RemoteException, RegionNotFoundException {
        try {
            this.setInUse();
            return this.rmsConnection.get(region, key);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public Map getRegionContent(Object region) throws RemoteException, RegionNotFoundException {
        try {
            this.setInUse();
            return this.rmsConnection.getRegionContent(region);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public byte[] getRegionContentAsBytes(Object region) throws RemoteException, RegionNotFoundException {
        try {
            this.setInUse();
            return this.rmsConnection.getRegionContentAsBytes(region);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public void remove(Object region, Object key) throws RemoteException, RegionNotFoundException, TimeoutException {
        try {
            this.setInUse();
            this.rmsConnection.remove(region, key);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public Set<Object> getKeys(Object region) throws RemoteException, RegionNotFoundException {
        try {
            this.setInUse();
            return this.rmsConnection.getKeys(region);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public Collection searchObjects(Object region, StorageFilter filter) throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.searchObjects(region, filter);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public Collection searchObjects(Collection<StorageFilter> filters) throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.searchObjects(filters);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public boolean containsObjects(Object region, StorageFilter filter) throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.containsObjects(region, filter);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public boolean containsObjects(Collection<StorageFilter> filters) throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.containsObjects(filters);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public int getObjectCount(Object region, StorageFilter filter) throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.getObjectCount(region, filter);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public int getObjectCount(Collection<StorageFilter> filters) throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.getObjectCount(filters);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public boolean hasRegion(Object region) throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.hasRegion(region);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public void createRegion(Object region) throws RemoteException, TimeoutException {
        try {
            this.setInUse();
            this.rmsConnection.createRegion(region);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public void clearRegion(Object region) throws RemoteException, TimeoutException {
        try {
            this.setInUse();
            this.rmsConnection.clearRegion(region);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public Set getRegions() throws RemoteException {
        try {
            this.setInUse();
            return this.rmsConnection.getRegions();
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public void removeRegion(Object region) throws RemoteException, TimeoutException {
        try {
            this.setInUse();
            this.rmsConnection.removeRegion(region);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public void lock(Object region, Object key) throws RemoteException, TimeoutException {
        try {
            this.setInUse();
            this.rmsConnection.lock(region, key);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    @Override
    public void unlock(Object region, Object key) throws RemoteException {
        try {
            this.setInUse();
            this.rmsConnection.unlock(region, key);
        } catch (IOException e) {
            notifyError(e);
            throw new RemoteException(e.getMessage(), e);
        } finally {
            try {
                this.close();
            } catch (ResourceException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    /**
     * Сообщает об ошибке на сторону АП-сервера
     *
     * @param e ошибка
     */
    private void notifyError(Exception e) {
//        ConnectionEvent event = new ConnectionEvent(managedConnection, ConnectionEvent.CONNECTION_ERROR_OCCURRED, e);

        managedConnection.notifyListeners(ConnectionEvent.CONNECTION_ERROR_OCCURRED, e);

//        for (ConnectionEventListener eventListener : managedConnection.getListeners()) {
//            eventListener.connectionErrorOccurred(event);
//        }
    }

    /**
     * Вызывает закрытие сессии на стороне RiMS-сервера
     *
     * @throws javax.resource.ResourceException при возникновении ошибки в процессе освобождения идентификатора сессии
     */
    void destroy() throws ResourceException {
        rmsConnection.close();
        inUse = 0;
    }

    /**
     * Инициирует комит на стороне RiMS-сервера
     *
     * @throws Exception при возникновении ошибки на стороне RiMS-сервера
     */
    @SuppressWarnings("WeakerAccess")
    public void commit() throws Exception {
        this.rmsConnection.commit();
    }

    /**
     * Инициирует откат на стороне RiMS-сервера
     *
     * @throws Exception при возникновении ошибки на стороне RiMS-сервера
     */
    @SuppressWarnings("WeakerAccess")
    public void rollback() throws Exception {
        this.rmsConnection.rollback();
    }

    /** Увеличивает счетчик использования "сессии" */
    public void setInUse() {

        inUse++;

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Wrapper increment usage [{0}], id[{1}]",
                    new Object[]{inUse, this.rmsConnection.getId().toString()});
        }
    }

    @Override
    public boolean isInUse() {
        return inUse > 0;
    }

    @Override
    public void addCloseTrigger(ConnectionEventListener listener) {
        managedConnection.addConnectionEventListener(listener);
    }

    @Override
    public void removeCloseTrigger(ConnectionEventListener listener) {
        managedConnection.removeConnectionEventListener(listener);
    }
}
