/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import javax.resource.spi.ConnectionRequestInfo;

/**
 * Пока маловнятная конструкция для попытки выбрать, возможно, более подходящий коннект
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSConnectionRequestInfo.java 84 2011-05-27 10:52:23Z dgomon $"
 */
public class RMSConnectionRequestInfo implements ConnectionRequestInfo {

    private final RMSConnectionSpec rmsConnectionSpec;

    public RMSConnectionRequestInfo(RMSConnectionSpec rmsConnectionSpec) {
        this.rmsConnectionSpec = rmsConnectionSpec;
    }

    @Override
    public int hashCode() {
        return rmsConnectionSpec.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        return obj instanceof RMSConnectionRequestInfo
                && rmsConnectionSpec.equals(((RMSConnectionRequestInfo) obj).rmsConnectionSpec);

    }

    public RMSConnectionSpec getRmsConnectionSpec() {
        return rmsConnectionSpec;
    }

}
