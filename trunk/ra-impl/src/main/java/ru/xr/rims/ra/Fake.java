/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra;

import javax.resource.ResourceException;
import javax.resource.cci.ConnectionMetaData;
import javax.resource.cci.ResourceAdapterMetaData;
import javax.resource.spi.ManagedConnectionMetaData;

/**
 * Единое место хранения фэйковых данных
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Fake.java 58 2010-11-07 20:22:56Z dgomon $"
 */
public interface Fake {

    /** Наименование кеша */
    String EIS_PRODUCT_NAME = "RiMS (Remote in Memory Storage)";
    /** Версия кеша */
    String EIS_PRODUCT_VERSION = "0.1";
    /** Фэйковый юзер */
    String EIS_USER_NAME = "RMSUser";

    /** Версия адаптера */
    String ADAPTER_VERSION = "0.1";
    /** Вендор адаптера */
    String ADAPTER_VENDOR = "xr.ru";
    /** Наименование адаптера */
    String ADAPTER_NAME = "RMS_RA";
    /** Краткое описание адаптера */
    String ADAPTER_SHORT_DESCRIPTION = "Remote in Memory Storage RA";

    /** Мета для коннекшена с кешом */
    ConnectionMetaData RMS_CONNECTION_META_DATA = new ConnectionMetaData() {
        @Override
        public String getEISProductName() throws ResourceException {
            return EIS_PRODUCT_NAME;
        }

        @Override
        public String getEISProductVersion() throws ResourceException {
            return EIS_PRODUCT_VERSION;
        }

        @Override
        public String getUserName() throws ResourceException {
            return EIS_USER_NAME;
        }
    };

    /** Мета для пулледконекшена с кешом */
    ManagedConnectionMetaData MANAGED_CONNECTION_META_DATA = new ManagedConnectionMetaData() {
        @Override
        public String getEISProductName() throws ResourceException {
            return EIS_PRODUCT_NAME;
        }

        @Override
        public String getEISProductVersion() throws ResourceException {
            return EIS_PRODUCT_VERSION;
        }

        @Override
        public int getMaxConnections() throws ResourceException {
            return 100;
        }

        @Override
        public String getUserName() throws ResourceException {
            return EIS_USER_NAME;
        }
    };

    /** Мета для ресурсадаптера */
    ResourceAdapterMetaData RESOURCE_ADAPTER_META_DATA = new ResourceAdapterMetaData() {

        @Override
        public String getAdapterVersion() {
            return ADAPTER_VERSION;
        }

        @Override
        public String getAdapterVendorName() {
            return ADAPTER_VENDOR;
        }

        @Override
        public String getAdapterName() {
            return ADAPTER_NAME;
        }

        @Override
        public String getAdapterShortDescription() {
            return ADAPTER_SHORT_DESCRIPTION;
        }

        @Override
        public String getSpecVersion() {
            return "1.5";
        }

        @Override
        public String[] getInteractionSpecsSupported() {
            return new String[]{"1.5"};
        }

        @Override
        public boolean supportsExecuteWithInputAndOutputRecord() {
            return false;
        }

        @Override
        public boolean supportsExecuteWithInputRecordOnly() {
            return true;
        }

        @Override
        public boolean supportsLocalTransactionDemarcation() {
            return true;
        }
    };

}
