/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import ru.xr.rims.services.RemoteStorage;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.spi.ConnectionEvent;
import javax.resource.spi.ConnectionEventListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Обертка для оптимизации получения соединения с кешом. Использует {@link ThreadLocal} для хранения подключения.
 * Если в одном треде происходит последовательность вложенных обращений к кешу, то будет использовано одно подключение.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RiMSLocalConnectionFactory.java 86 2011-06-02 09:08:25Z dgomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class RiMSLocalConnectionFactory {

    /** JNDI-имя для поиска ConnectionFactory к кешу */
    @SuppressWarnings("WeakerAccess")
    public static final String RMSCONNECTION_FACTORY_JNDI_ADDRESS = "ru/xr/RMSConnectionFactory";

    /** Хранилище для полученного подключения */
    private static ThreadLocal<RMSConnection> connectionThreadLocal = new ThreadLocal<>();

    private static final Logger logger = Logger.getLogger(RiMSLocalConnectionFactory.class.getName());

    /**
     * Метод возвращает конекшен к кешу.
     * В случае присутствия оного в ThreadLocal будет использован имеющийса, в противном случае, будет создан новый,
     * размещен в ThreadLocal и возвращен в качестве результата вызова метода
     *
     * @return подключение к кешу
     * @throws javax.naming.NamingException при невозможности обнаружения
     * ConnectionFactory по {@link #RMSCONNECTION_FACTORY_JNDI_ADDRESS адресу}
     * @throws javax.resource.ResourceException при ошибке получения ресурса от сервера приложений
     */
    public static RMSConnection getConnection() throws ResourceException, NamingException {

        RMSConnection connection = connectionThreadLocal.get();
        if (connection == null || !connection.isInUse()) {
            logger.fine("Create new...");

            if (connection != null) {
                connectionThreadLocal.remove();
            }

            connection = obtanj2eeConnection();
            connection.addCloseTrigger(new CloseTrigger());
            connectionThreadLocal.set(connection);
        } else {
            logger.fine("Use old...");
        }

        return connection;
    }

    public static void parkConnection(RemoteStorage remoteStorage) {
        try {
            if (logger.isLoggable(Level.FINE)) {
                logger.fine("Park connection: " + remoteStorage);
            }

            ((RMSConnection) remoteStorage).setInUse();
        } catch (ClassCastException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public static void unParkConnection(RemoteStorage remoteStorage) throws ResourceException {
        try {
            if (logger.isLoggable(Level.FINE)) {
                logger.fine("unPark connection: " + remoteStorage);
            }

            ((RMSConnection) remoteStorage).close();
        } catch (ClassCastException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Получает из JNDI-контекста ссылку на ConnectionFactory и из нее RMSConnection для сервера кеша
     *
     * @return подключение к кешу
     * @throws javax.naming.NamingException при невозможности обнаружения
     * ConnectionFactory по {@link #RMSCONNECTION_FACTORY_JNDI_ADDRESS адресу}
     * @throws javax.resource.ResourceException при ошибке получения ресурса от сервера приложений
     */
    private static RMSConnection obtanj2eeConnection() throws NamingException, ResourceException {

        ConnectionFactory connectionFactory =
                (ConnectionFactory) new InitialContext().lookup(RMSCONNECTION_FACTORY_JNDI_ADDRESS);

        return (RMSConnection) connectionFactory.getConnection();
    }


    private static class CloseTrigger implements ConnectionEventListener {

        @Override
        public void connectionClosed(ConnectionEvent connectionEvent) {

            final RMSConnection rmsConnection = connectionThreadLocal.get();

            if (rmsConnection != null) {
                connectionThreadLocal.remove();
                rmsConnection.removeCloseTrigger(this);

                if (logger.isLoggable(Level.FINE)) {
                    logger.fine("Close event, ID: [" + connectionEvent.getId() +
                            "] Local connection instance are removed...");
                }

            }
        }

        // методы не используются...
        @Override
        public void localTransactionStarted(ConnectionEvent connectionEvent) {
        }

        @Override
        public void localTransactionCommitted(ConnectionEvent connectionEvent) {
        }

        @Override
        public void localTransactionRolledback(ConnectionEvent connectionEvent) {
        }

        @Override
        public void connectionErrorOccurred(ConnectionEvent connectionEvent) {
        }

    }

}
