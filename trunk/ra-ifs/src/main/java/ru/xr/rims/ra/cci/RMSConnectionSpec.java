/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import javax.resource.cci.ConnectionSpec;

/**
 * Пока маловнятная конструкция для попытки выбрать, возможно, более подходящий коннект
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSConnectionSpec.java 83 2011-05-27 10:51:11Z dgomon $"
 */
public class RMSConnectionSpec implements ConnectionSpec {

    private Object connectionId;
    private Object xid;

    public Object getConnectionId() {
        return connectionId;
    }

    @SuppressWarnings("unused")
    public void setConnectionId(Object connectionId) {
        this.connectionId = connectionId;
    }

    @SuppressWarnings("WeakerAccess")
    public Object getXid() {
        return xid;
    }

    @SuppressWarnings("unused")
    public void setXid(Object xid) {
        this.xid = xid;
    }

    @Override
    public int hashCode() {

        int result = 0;

        if (connectionId != null) {
            result = result + 31 * connectionId.hashCode();
        }

        if (xid != null) {
            result = result + 31 * xid.hashCode();
        }

        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RMSConnectionSpec)) {
            return false;
        }

        RMSConnectionSpec other = (RMSConnectionSpec) obj;

        if (this.connectionId != null && this.connectionId.equals(other.getConnectionId())) {
            if (this.xid != null && this.xid.equals(other.getXid())) {
                return true;
            }
        }

        return false;
    }
}
