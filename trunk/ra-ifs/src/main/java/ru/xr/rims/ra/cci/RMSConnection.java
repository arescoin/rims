/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.ra.cci;

import ru.xr.rims.services.RemoteStorage;

import javax.resource.cci.Connection;
import javax.resource.spi.ConnectionEventListener;


/**
 * Интерфейс предназначен для применения в ресурсадаптере
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RMSConnection.java 77 2011-05-06 08:02:19Z dgomon $"
 */
public interface RMSConnection extends Connection, RemoteStorage {

    /** Устанавливает признак использования коннекции */
    void setInUse();

    /**
     * Возвращает признак использования данного соединения
     *
     * @return true - используется, иначе - false
     */
    boolean isInUse();

    /**
     * Возвращает идентификатор подключения
     *
     * @return идентификатор
     */
    Object getId();

    /**
     * Добавляет листенер на закрытие соединения
     *
     * @param listener код должен отрабатывать при закрытии соединения
     */
    void addCloseTrigger(ConnectionEventListener listener);

    /**
     * Удаляет листенер на закрытие соединения
     *
     * @param listener удаляемый листенер
     */
    void removeCloseTrigger(ConnectionEventListener listener);
}
