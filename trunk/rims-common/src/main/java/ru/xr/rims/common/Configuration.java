/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.common;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Конфигурация приложения
 * <p/>
 * Зачитывает и разбирает параметры конфигурационного файла (*.properties - формат), делая их доступными для приложения
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Configuration.java 60 2010-11-15 12:14:01Z dgomon $"
 */
public class Configuration {

    private static Configuration instance;

    /** ключ для указания текущего сервера */
    @SuppressWarnings("WeakerAccess")
    public static final String CURRENT_SERVER = "current";
    /** префикс ключа для указания сетевых параметров сервера */
    @SuppressWarnings("WeakerAccess")
    public static final String SERVER_ITEM = "server.";
    /** ключ для указания периода опроса "соседних" серверов */
    @SuppressWarnings("WeakerAccess")
    public static final String MONITOR_PERIOD = "monitor.period";
    /** ключ для указания времени ожидания постановки блокировки на объекте */
    @SuppressWarnings("WeakerAccess")
    public static final String LOC_REQUEST_TIMEOUT = "locRequest.timeOut";
    /** ключ для указания периодичности обновления удаленного объекта в Registry */
    @SuppressWarnings("WeakerAccess")
    public static final String REBIND_PERIOD = "rebind.period";

    /** Значение по умолчанию для периода опроса "соседних" серверов */
    @SuppressWarnings("WeakerAccess")
    public static final long DEFAULT_MONITOR_PERIOD = 1000;
    /** Значение по умолчанию для времени ожидания постановки блокировки на объекте */
    @SuppressWarnings("WeakerAccess")
    public static final long DEFAULT_LOC_REQUEST_TIMEOUT = 30000;
    /** Значение по умолчанию для периодичности обновления удаленного объекта в Registry */
    @SuppressWarnings("WeakerAccess")
    public static final long DEFAULT_REBIND_PERIOD = 300000;

    /** проперти основной конфиги, из файла */
    private Properties fileProperties = new Properties();
    /** хранилище для свойств выставленных программным путем (не из файла) */
    private Properties customProperties = new Properties();

    /** Имя текущего сервера */
    private String currentServerName;
    /** Период опроса серверов */
    private long monitorPeriod;
    /** Ожидание блокировки для сервера по-умолчанию */
    private long locRequestTimeOut;
    /** Период переустановки объектов в JNDI */
    private long rebindPeriod;

    private HashMap<String, String> servers = new HashMap<>();

    private static Logger logger = Logger.getLogger(Configuration.class.getName());

    /**
     * Конструирует и настраивает инстанс
     *
     * @param configFileName путь к конфигу
     * @throws java.io.IOException в случае ошибки чтения конфига
     */
    private Configuration(String configFileName) throws IOException {

        File configFile = new File(configFileName);
        if (!configFile.exists() || !configFile.isFile() || !configFile.canRead()) {
            throw new IllegalStateException("Cant read configuration [" + configFile.getAbsolutePath() + "]");
        }

        this.fileProperties.load(new FileInputStream(configFile));

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        printWriter.println();
        printWriter.println("==========================================================");
        printWriter.append("Read configuration from file [");
        printWriter.append(configFile.getAbsolutePath());
        printWriter.append("]");
        printWriter.println();
        this.fileProperties.list(printWriter);
        printWriter.println("==========================================================");
        logger.info(stringWriter.toString());
        stringWriter.close();
        printWriter.close();

        prepareConfiguration();
    }

    /**
     * Возвращает подготовленный инстанс конфигурации
     *
     * @return готовая конфига
     */
    public static Configuration getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Not initialized");
        }
        return instance;
    }

    /**
     * Возвращает признак готовности конфига
     *
     * @return true - готов, иначе - false
     */
    public static boolean isInitialized() {
        return instance != null;
    }

    /**
     * Выполняет инициализацию конфига свойствами из указанного файла
     *
     * @param configFileName путь к конфиг-файлу
     * @throws java.io.IOException при ошибке доступа к файлу
     */
    public static void initialize(String configFileName) throws IOException {

        if (instance != null) {
            throw new IllegalStateException("Configuration already initialized");
        }

        if (configFileName == null || configFileName.length() < 1) {
            throw new IllegalArgumentException("Incorrect file name [" + configFileName + "]");
        }

        instance = new Configuration(configFileName);
    }

    /**
     * Производит повторную инициализацию конфига
     *
     * @param configFileName путь к конфиг-файлу
     * @throws java.io.IOException путь к конфиг-файлу
     */
    public synchronized static void reInit(String configFileName) throws IOException {

        if (configFileName == null || configFileName.length() < 1) {
            throw new IllegalArgumentException("Incorrect file name [" + configFileName + "]");
        }

        instance = new Configuration(configFileName);
    }

    /** Последовательно инициализирует свойства конфигурации */
    @SuppressWarnings("WeakerAccess")
    protected void prepareConfiguration() {
        prepareServerItems();
        prepareCurrentServer();
        prepareMonitorPeriod();
        prepareLocRequestTimeOut();
        prepareRebindPeriod();
    }

    /** Устанавливает значение текущего сервера. Если сервер не указан в ручню, используется значение из файла */
    private void prepareCurrentServer() {
        this.currentServerName = this.customProperties.getProperty(CURRENT_SERVER);
        if (this.currentServerName == null) {
            this.currentServerName = this.fileProperties.getProperty(CURRENT_SERVER);
        }

        if (!this.servers.containsKey(this.currentServerName)) {
            throw new IllegalArgumentException("Incorrect current server");
        }
    }

    /** Устанавливает значение периода опроса "соседних" серверов */
    private void prepareMonitorPeriod() {
        String defMonitor = this.fileProperties.getProperty(MONITOR_PERIOD, DEFAULT_MONITOR_PERIOD + "");
        if (defMonitor == null) {
            throw new IllegalArgumentException("Incorrect " + MONITOR_PERIOD + " parameter, found null.");
        }

        defMonitor = defMonitor.trim();
        if (defMonitor.length() < 1) {
            throw new IllegalArgumentException("Incorrect " + MONITOR_PERIOD + " parameter, empty.");
        }

        try {
            this.monitorPeriod = Long.parseLong(defMonitor);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect " + MONITOR_PERIOD + " parameter [" + defMonitor + "]");
        }
    }

    /** Устанавливает значение времени ожидания постановки блокировки на объекте */
    private void prepareLocRequestTimeOut() {
        String rqTimeoutStr =
                this.fileProperties.getProperty(LOC_REQUEST_TIMEOUT, DEFAULT_LOC_REQUEST_TIMEOUT + "").trim();

        if (rqTimeoutStr.length() < 1) {
            throw new IllegalArgumentException("Incorrect " + LOC_REQUEST_TIMEOUT + " parameter, empty.");
        }

        try {
            this.locRequestTimeOut = Long.parseLong(rqTimeoutStr);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect " + LOC_REQUEST_TIMEOUT + " parameter [" + rqTimeoutStr + "]");
        }
    }

    /** Устанавливает значение периодичности обновления удаленного объекта в Registry */
    private void prepareRebindPeriod() {
        String rebindPeriod =
                this.fileProperties.getProperty(REBIND_PERIOD, DEFAULT_REBIND_PERIOD + "").trim();

        if (rebindPeriod.length() < 1) {
            throw new IllegalArgumentException("Incorrect " + REBIND_PERIOD + " parameter, empty.");
        }

        try {
            this.rebindPeriod = Long.parseLong(rebindPeriod);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect " + rebindPeriod + " parameter [" + rebindPeriod + "]");
        }
    }

    /** Подготавливает перечисление доступных серверов */
    private void prepareServerItems() {
        for (String key : this.fileProperties.stringPropertyNames()) {
            if (key.startsWith(SERVER_ITEM)) {
                String serverString = this.fileProperties.getProperty(key);
                if (serverString == null || serverString.trim().length() < 5) {
                    throw new IllegalArgumentException("Incorrect server description by key [" + key + "]");
                }
                this.servers.put(key, serverString);
            }
        }

        if (this.servers.isEmpty()) {
            throw new IllegalStateException("Configuration file doe's not contains valid server");
        }
    }

    /**
     * Устанавливает значение для текущего сервера
     *
     * @param currentServer ключ сервера
     * @throws IllegalArgumentException при указании несуществующего ключа
     */
    @SuppressWarnings("WeakerAccess")
    public void setCurrentServer(String currentServer) throws IllegalArgumentException {
        if (!this.servers.containsKey(currentServer)) {
            throw new IllegalArgumentException("Incorrect current server");
        }

        this.customProperties.setProperty(CURRENT_SERVER, currentServer);
        this.currentServerName = currentServer;
    }

    /**
     * Возвращает ключ (наименование) текущего сервера
     *
     * @return ключ
     */
    @SuppressWarnings("WeakerAccess")
    public String getCurrentServer() {
        return this.currentServerName;
    }

    /**
     * Возвращает значение периода опроса "соседних" серверов
     *
     * @return периодичность в <i>мс</i>
     */
    @SuppressWarnings("WeakerAccess")
    public long getMonitorPeriod() {
        return this.monitorPeriod;
    }

    /**
     * Возвращает значение времени ожидания постановки блокировки на объекте
     *
     * @return ожидание в <i>мс</i>
     */
    public long getLocRequestTimeout() {
        return this.locRequestTimeOut;
    }

    /**
     * Возвращает значение периодичности обновления удаленного объекта в Registry
     *
     * @return периодичность в <i>мс</i>
     */
    public long getRebindPeriod() {
        return rebindPeriod;
    }

    /**
     * Немодифицируемая карта с сетевыми конфигами серверов
     *
     * @return карта, в ключе идентификатор, в значении - конфиг
     */
    public Map<String, String> getServers() {
        return Collections.unmodifiableMap(this.servers);
    }
}

