/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.common;

import ru.xr.rims.services.GlobalService;
import ru.xr.rims.services.ServerStorage;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Хранилище сереверов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ServerArray.java 93 2011-08-26 07:51:37Z dgomon $"
 */
public class ServerArray {

    private static Logger logger = Logger.getLogger(ServerArray.class.getName());

    private static final ServerArray ARRAY = new ServerArray();

    /** Период опроса серверов */
    private static long monitorPeriod;

    /** Содержит все сервера */
    private final Set<Server> servers = new HashSet<>();
    /** Содержит доступные сервера */
    private final Set<Server> onlineServers = new HashSet<>();

    /** Признак активности монитора серверов */
    private boolean active = true;
    /** Текущий сервер */
    private Server current;

    /**
     * Возвращает инстанс хранилища серверов
     *
     * @return хранилище
     */
    public static ServerArray getArray() {
        return ARRAY;
    }

    /** Конструирует хранилище */
    private ServerArray() {

        String currentServerKey = Configuration.getInstance().getCurrentServer();
        for (Object o : Configuration.getInstance().getServers().keySet()) {
            String key = (String) o;
            String serverConfig = Configuration.getInstance().getServers().get(key);
            if (!key.equals(currentServerKey)) {
                Server other = constructServer(serverConfig);
                servers.add(other);
            } else {
                current = constructServer(serverConfig);
            }
        }

        if (current == null) {
            logger.info("Error: Current server incorrect! Now Exit!");
            System.exit(202);
        }

        logger.info("Array created...");
    }

    /** Выставляет признак выключения мониторинга серверов */
    public void stopMonitor() {
        this.active = false;
    }


    /** Создает и запускает мониторинг серверов */
    public void startMonitor() {

        this.active = true;

        ServerMonitor monitor = new ServerMonitor();
        new Thread(monitor).start();
        while (monitor.isStarted()) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace(System.err);
            }
        }
    }

    /**
     * Конструирует инстанс сервера, по переданной строке
     *
     * @param serverString сетевая конфигурация сервера
     * @return инстанс сервера
     */
    private Server constructServer(String serverString) {

        String[] parts = serverString.split(":");
        if (parts.length != 5) {
            throw new IllegalArgumentException("Incorrect server configuration [" + serverString + "]");
        }

        for (int i = 0; i < parts.length; i++) {
            parts[i] = parts[i].trim();
            if (parts[i].length() < 1) {
                throw new IllegalArgumentException("Incorrect server configuration [" + serverString + "]");
            }
        }

        String idString = parts[0];
        String hostClient = parts[1];
        String portClientString = parts[2];
        String hostServer = parts[3];
        String portServerString = parts[4];

        int id = Integer.parseInt(idString);
        int portClient = Integer.parseInt(portClientString);
        int portServer = Integer.parseInt(portServerString);

        return new Server(id, hostClient, portClient, hostServer, portServer);
    }

    /**
     * Возвращает немодифицируемое перечисление сконфигурированных серверов
     *
     * @return сервера
     */
    public Set<Server> getServers() {
        return Collections.unmodifiableSet(servers);
    }

    /**
     * Возвращает немодифицируемое перечисление доступных серверов.
     * Формируется по результатам мониторинга.
     *
     * @return сервера
     */
    public Set<Server> getServersOnLine() {
        return Collections.unmodifiableSet(onlineServers);
    }

    /**
     * Возвращает инстанс текущего сервера
     *
     * @return текущий сервер
     */
    public Server getCurrent() {
        return current;
    }

    /** Сервер приложения */
    public class Server {

        /** Идентификатор сервера */
        private final int id;
        /** Ност интерфейса для клиентской части */
        private final String hostClient;
        /** Ност интерфейса для серверной части */
        private final String hostServer;
        /** Порт клиентской части */
        private final int portClient;
        /** Порт серверной части */
        private final int portServer;
        /** Клиентский реестр */
        private Registry clientRegistry;
        /** Серверный реестр */
        private Registry serverRegistry;

        /** Строковое предстанление */
        private final String stringView;

        /**
         * Конструирует новый инстанс сервера
         *
         * @param id         идентификатор сервера
         * @param hostClient имя хоста для принятия клиентских запросов
         * @param portClient клиентский порт
         * @param hostServer имя хоста для принятия серверных запросов
         * @param portServer серверный порт
         */
        public Server(int id, String hostClient, int portClient, String hostServer, int portServer) {
            this.id = id;
            this.hostClient = hostClient;
            this.portClient = portClient;
            this.hostServer = hostServer;
            this.portServer = portServer;

            stringView = this.id + ":"
                    + this.hostClient + ":" + this.portClient + ":"
                    + this.hostServer + ":" + this.portServer;

            logger.info("Server created: " + this.toString());
        }

        public int getId() {
            return id;
        }

        public String getHostClient() {
            return hostClient;
        }

        public int getPortClient() {
            return portClient;
        }

        public String getHostServer() {
            return hostServer;
        }

        public int getPortServer() {
            return portServer;
        }

        /**
         * Инициализирует клиентский и серверный реестры
         *
         * @throws java.rmi.RemoteException при невозможности установить сервера с требуемыми параметрами
         */
        public void initRegistry() throws RemoteException {
            initRegistry(false);
        }

        /**
         * Инициализирует реестры
         *
         * @param clientOnly true - только клиентский, false - клиентский и серверный реестры
         * @throws java.rmi.RemoteException при невозможности установить сервера с требуемыми параметрами
         */
        public void initRegistry(boolean clientOnly) throws RemoteException {
            clientRegistry = LocateRegistry.getRegistry(hostClient, portClient);
            if (!clientOnly) {
                serverRegistry = LocateRegistry.getRegistry(hostServer, portServer);
            }
        }

        public GlobalService getRemoteCache() throws RemoteException, NotBoundException {
            return (GlobalService) clientRegistry.lookup(GlobalService.REMOTE_ELEMENT);
        }

        public ServerStorage getServerCache() throws RemoteException, NotBoundException {
            return (ServerStorage) serverRegistry.lookup(ServerStorage.REMOTE_ELEMENT);
        }

        @Override
        public String toString() {
            return stringView;
        }
    }

    private class ServerMonitor implements Runnable {

        private ServerMonitor() {
            try {
                monitorPeriod = Configuration.getInstance().getMonitorPeriod();
            } catch (NumberFormatException e) {
                logger.info("Error: incorrect MONITOR_PERIOD? use default...");
            }
        }

        private boolean started = false;

        @Override
        public void run() {
            while (ServerArray.this.active) {
                for (Server server : servers) {
                    String pong = null;
                    try {
                        pong = server.getServerCache().ping();
                    } catch (RemoteException | NotBoundException e) {
                        logger.log(Level.FINE, e.getMessage(), e);
                    }

                    if (ServerStorage.PING.equals(pong)) {
                        onlineServers.add(server);
                        logger.info("Server online: " + server.getHostClient() + ":" + server.getPortClient());
                    } else {
                        onlineServers.remove(server);
                        logger.info("Server offline: " + server.getHostClient() + ":" + server.getPortClient());
                    }
                }

                started = true;
                try {
                    Thread.sleep(monitorPeriod);
                } catch (InterruptedException e) {
                    if (logger.isLoggable(Level.FINEST)) {
                        logger.log(Level.FINEST, e.getMessage(), e);
                    } else {
                        logger.info(e.getMessage());
                    }
                }
            }
        }

        boolean isStarted() {
            return this.started;
        }
    }

}
