/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.rims.common;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Базовый класс для приложений предоставляющих сервис удаленного кеша и работающих с этим сервисом
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Application.java 93 2011-08-26 07:51:37Z dgomon $"
 */
public abstract class Application {

    /** Имя конфиг-файла по-умолчанию */
    @SuppressWarnings("WeakerAccess")
    public static final String CONFIG_FILENAME = "servers.properties";

    /** Разделитель для параметра сетевой конфигурации серверов */
    public static final String PARAM_DELIMETER = "=";

    /** Используемый файл конфигурации */
    private static String configFileName;
    /** Имя рабочего сервера */
    private static String currentServerName;

    private static Logger logger = Logger.getLogger(Application.class.getName());

    /**
     * Разбирает массив на аргументы запуска приложения
     *
     * @param args аргументы
     */
    protected void parseArgs(String[] args) {

        logger.info("Available arguments: " + Arrays.toString(Argument.values()));
        logger.info("System starting with arguments: " + Arrays.toString(args));

        if (Argument.values().length < args.length) {
            exit(ExitForCode.errorArguments, "To many arguments");
        }

        for (String arg : args) {
            String[] argParts = arg.split(PARAM_DELIMETER);
            if (argParts.length != 2) {
                exit(ExitForCode.errorArguments, "Incorrect argument - " + arg);
            }

            String argumentStr = argParts[0].trim();
            String valueStr = argParts[1].trim();

            if (argumentStr.length() < 1 || valueStr.length() < 1) {
                exit(ExitForCode.errorArguments, "Incorrect arguments");
            }

            try {
                Argument argument = Argument.valueOf(argumentStr);
                argument.setValue(valueStr);
            } catch (IllegalArgumentException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                exit(ExitForCode.errorArguments, "Incorrect argument - " + arg);
            }
        }

        for (Argument argument : Argument.values()) {
            switch (argument) {
            case cf:
                configFileName = argument.getValue();
                break;
            case cs:
                currentServerName = argument.getValue();
                break;
            default:
                exit(ExitForCode.errorArguments, "Unknown argument:" + argument);
            }
        }
    }

    /**
     * Инициализирует истанс приложения, используя системный файл конфигурации
     *
     * @throws java.rmi.RemoteException при невозможности создать ссылку на реестры серверов
     */
    protected void init() throws RemoteException {
        init(configFileName);
    }

    /**
     * Инициализирует истанс приложения, используя указанный файл конфигурации
     *
     * @param configFileName путь к конфигу
     * @throws java.rmi.RemoteException при невозможности создать ссылку на реестры серверов
     */
    protected void init(String configFileName) throws RemoteException {
        logger.info("Application initializing, configuration at [" + configFileName + "]");
        initProps(configFileName);
        initServerArray();
    }

    /**
     * Инициализирует конфигурацию из свойств файла по указанному пути
     *
     * @param configFileName путь к конфигам
     */
    @SuppressWarnings("WeakerAccess")
    protected void initProps(String configFileName) {
        try {
            if (!Configuration.isInitialized()) {
                Configuration.initialize(configFileName);
            }
            // Если указали конкретный сервер при запуске, то установим его на конфиге... 
            if (currentServerName != null && currentServerName.trim().length() > 0) {
                Configuration.getInstance().setCurrentServer(currentServerName);
            }
        } catch (IOException e) {
            exit(ExitForCode.errorConfig, e.getMessage());
        }
    }

    /**
     * Инициализирует массив серверов
     *
     * @throws java.rmi.RemoteException при невозможности создать ссылку на реестры серверов
     */
    @SuppressWarnings("WeakerAccess")
    protected void initServerArray() throws RemoteException {

        ServerArray serverArray = ServerArray.getArray();
        for (ServerArray.Server server : serverArray.getServers()) {
            server.initRegistry(false);
        }

        ServerArray.Server server = serverArray.getCurrent();
        logger.info("Current server:");
        logger.info("  Server host [" + server.getHostServer() + "]");
        logger.info("  Server port [" + server.getPortServer() + "]");
        logger.info("  Client host [" + server.getHostClient() + "]");
        logger.info("  Client port [" + server.getPortClient() + "]");
    }

    /** Метод отрабатывает при поступлении команды на завершение работы приложения */
    protected void stop() {
        logger.info("Application down complete.");
    }

    /**
     * Команда на завершение работы приложения
     *
     * @param code код выхода
     */
    protected void exit(ExitForCode code) {
        exit(code, null);
    }

    /**
     * Команда на завершение работы приложения
     *
     * @param code    код выхода
     * @param message дополнительное сообщение
     */
    public void exit(ExitForCode code, String message) {
        stop();
        code.exit(message);
    }

    /** Перечисление поддерживаемых аргументов командной строки */
    public enum Argument {
        /** Config fileName */
        cf(CONFIG_FILENAME),
        /** Current server name */
        cs("");

        private String value;

        Argument(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
