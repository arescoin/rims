/*
 *
 *  Copyright (c) 2017, Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 *
 */

package ru.xr.cache;

/**
 * Указывает на попытку обращения к несуществующему региону
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RegionNotFoundException.java 163 2014-07-10 09:31:29Z DGomon $"
 */
public class RegionNotFoundException extends RuntimeException {
    public RegionNotFoundException(String message) {
        super(message);
    }
}
